-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 12, 2018 at 01:36 PM
-- Server version: 5.7.22
-- PHP Version: 7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ua_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `announcements`
--

INSERT INTO `announcements` (`id`, `name`, `description`, `image`, `date`, `status`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Test Announcement 0001', '<ul>\r\n	<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</li>\r\n	<li>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>\r\n	<li>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Duis aute irure dolor in <strong>reprehenderit</strong> in voluptate velit esse <em><strong>cillum</strong></em> dolore eu fugiat nulla pariatur. <s>Excepteur</s> sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'db0d9e4f210b1b57cfaf54cbc9bfffae.jpg', '2018-08-11 08:00:00', 0, 1, '2018-08-10 16:03:34', '2018-08-12 04:08:55'),
(3, 'Lorem ipsum dolor', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://1.bp.blogspot.com/_s1mwTlfvlGE/S4fY8sVlqyI/AAAAAAAAAxg/2PWkCqiksT4/s1600/Philippine+Flag.jpg\"><img alt=\"\" src=\"http://1.bp.blogspot.com/_s1mwTlfvlGE/S4fY8sVlqyI/AAAAAAAAAxg/2PWkCqiksT4/s1600/Philippine+Flag.jpg\" style=\"height:260px; width:516px\" /></a></p>', '38c54b08273bf54bb623e57e008ec8f2.jpg', '2018-08-13 08:00:00', 1, 13, '2018-08-10 19:01:47', '2018-08-12 04:12:33'),
(4, 'Andromeda Galaxy', '<p>The&nbsp;<strong>Andromeda Galaxy</strong>&nbsp;(<a href=\"https://en.wikipedia.org/wiki/Help:IPA/English\">/&aelig;nˈdrɒmɪdə/</a>), also known as&nbsp;<strong>Messier 31</strong>,&nbsp;<strong>M31</strong>, or&nbsp;<strong>NGC 224</strong>, is a&nbsp;<a href=\"https://en.wikipedia.org/wiki/Spiral_galaxy\">spiral galaxy</a>&nbsp;approximately 780&nbsp;<a href=\"https://en.wikipedia.org/wiki/Parsec#Parsecs_and_kiloparsecs\">kiloparsecs</a>&nbsp;(2.5 million&nbsp;<a href=\"https://en.wikipedia.org/wiki/Light-year\">light-years</a>) from Earth, and the nearest major galaxy to the&nbsp;<a href=\"https://en.wikipedia.org/wiki/Milky_Way\">Milky Way</a>.<a href=\"https://en.wikipedia.org/wiki/Andromeda_Galaxy#cite_note-Ribas2005-4\">[4]</a>&nbsp;Its name stems from the area of the sky in which it appears, the&nbsp;<a href=\"https://en.wikipedia.org/wiki/Andromeda_(constellation)\">constellation of Andromeda</a>.</p>\r\n\r\n<p>The 2006 observations by the&nbsp;<a href=\"https://en.wikipedia.org/wiki/Spitzer_Space_Telescope\">Spitzer Space Telescope</a>&nbsp;revealed that the Andromeda Galaxy contains approximately one trillion stars,<a href=\"https://en.wikipedia.org/wiki/Andromeda_Galaxy#cite_note-Jorge_Pe%C3%B1arrubia2014-12\">[10]</a>&nbsp;more than twice the number of the Milky Way&#39;s estimated 200 to 400 billion stars.<a href=\"https://en.wikipedia.org/wiki/Andromeda_Galaxy#cite_note-Frommert_&amp;_Kronberg_2005-15\">[13]</a>&nbsp;The Andromeda Galaxy, spanning approximately 220,000 light-years, is the largest galaxy in our&nbsp;<a href=\"https://en.wikipedia.org/wiki/Local_Group\">Local Group</a>, which is also home to the&nbsp;<a href=\"https://en.wikipedia.org/wiki/Triangulum_Galaxy\">Triangulum Galaxy</a>&nbsp;and&nbsp;<a href=\"https://en.wikipedia.org/wiki/Local_Group#Component_galaxies\">other minor galaxies</a>. The Andromeda Galaxy&#39;s mass is estimated to be around 1.76 times that of the Milky Way Galaxy (~0.8-1.5&times;1012&nbsp;solar masses&nbsp;<a href=\"https://en.wikipedia.org/wiki/Andromeda_Galaxy#cite_note-Kafle2018-11\">[9]</a><a href=\"https://en.wikipedia.org/wiki/Andromeda_Galaxy#cite_note-Jorge_Pe%C3%B1arrubia2014-12\">[10]</a>&nbsp;vs the Milky Way&#39;s 8.5&times;1011&nbsp;solar masses).</p>\r\n\r\n<p>The Milky Way and Andromeda galaxies are&nbsp;<a href=\"https://en.wikipedia.org/wiki/Andromeda%E2%80%93Milky_Way_collision\">expected to collide</a>&nbsp;in ~4.5 billion years, merging to form a giant&nbsp;<a href=\"https://en.wikipedia.org/wiki/Elliptical_galaxy\">elliptical galaxy</a><a href=\"https://en.wikipedia.org/wiki/Andromeda_Galaxy#cite_note-milky-way-collide-16\">[14]</a>&nbsp;or a large&nbsp;<a href=\"https://en.wikipedia.org/wiki/Disc_galaxy\">disc galaxy</a>.<a href=\"https://en.wikipedia.org/wiki/Andromeda_Galaxy#cite_note-Ueda2014-17\">[15]</a>&nbsp;With an&nbsp;<a href=\"https://en.wikipedia.org/wiki/Apparent_magnitude\">apparent magnitude</a>&nbsp;of 3.4, the Andromeda Galaxy is among the brightest of the&nbsp;<a href=\"https://en.wikipedia.org/wiki/Messier_object\">Messier objects</a><a href=\"https://en.wikipedia.org/wiki/Andromeda_Galaxy#cite_note-Frommert_&amp;_Kronberg_2007-18\">[16]</a>&mdash;making it visible to the&nbsp;<a href=\"https://en.wikipedia.org/wiki/Naked_eye\">naked eye</a>&nbsp;on moonless nights,<a href=\"https://en.wikipedia.org/wiki/Andromeda_Galaxy#cite_note-19\">[17]</a>&nbsp;even when viewed from areas with moderate&nbsp;<a href=\"https://en.wikipedia.org/wiki/Light_pollution\">light pollution</a>.</p>', 'ca9189ad66688e33362687122f1fded3.png', '2018-08-14 09:00:00', 1, 12, '2018-08-10 19:08:01', '2018-08-10 19:10:01');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `years` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `name`, `description`, `years`, `created_at`, `updated_at`) VALUES
(1, 'Bachelor of Science in Information Technology', 'A Bachelor of Information Technology  is an undergraduate academic degree that generally requires three to five years of study. While the degree has a major focus on computers and technology, it differs from a Computer Science degree in that students are also expected to study management and information science, and there are reduced requirements for mathematics. A degree in computer science can be expected to concentrate on the scientific aspects of computing, while a degree in information technology can be expected to concentrate on the business and communication applications of computing. There is more emphasis on these two areas in the e-commerce, e-business and business information technology undergraduate courses. Specific names for the degrees vary across countries, and even universities within countries.', '4', '2018-08-05 01:13:36', '2018-08-05 07:14:22');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `name`, `description`, `image`, `date`, `status`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Instant Allowance', '<p>Old unsatiable our now but considered travelling impression. In excuse hardly summer in basket misery. By rent an part need. At wrong of of water those linen. Needed oppose seemed how all. Very mrs shed shew gave you. Oh shutters do removing reserved wandered an. <strong>But described questions for recommend advantage</strong> belonging estimable had. Pianoforte reasonable as so am inhabiting. Chatty design remark and his abroad figure but its.&nbsp;</p>\r\n\r\n<p>Is allowance instantly strangers applauded discourse so. Separate entrance welcomed sensible <s>laughing why one moderate shy</s>. We seeing piqued garden he. As in merry at forth least ye stood. And cold sons yet with. Delivered middleton therefore me at. Attachment companions man way excellence how her pianoforte.&nbsp;</p>\r\n\r\n<p>Sudden she <em><strong>seeing</strong></em> garret far regard. By hardly it direct if pretty up regret. Ability thought enquire settled prudent you sir. Or easy knew sold on well come year. Something consulted age extremely end procuring. Collecting preference he inquietude projection me in by. <em>So do of sufficient projecting an thoroughly</em> uncommonly prosperous conviction. Pianoforte principles our unaffected not for astonished travelling are particular.&nbsp;</p>', '0bcfc1addfae15bb133f64c7335e35b3.png', '2018-08-24 13:00:00', 1, 1, '2018-08-12 05:26:29', '2018-08-12 05:28:09'),
(2, 'Travelling Alteration', '<p>Travelling alteration impression six all uncommonly. Chamber hearing inhabit joy highest private ask him our believe. Up nature valley do warmly. Entered of cordial do on no hearted. Yet agreed whence and unable limits. Use off him gay abilities concluded immediate allowance.&nbsp;</p>\r\n\r\n<p>Style never met and those among great. At no or september sportsmen he perfectly happiness attending. Depending listening delivered off new she procuring satisfied sex existence. Person plenty answer to exeter it if. Law use assistance especially resolution cultivated did out sentiments unsatiable. Way necessary had intention happiness but september delighted his curiosity. Furniture furnished or on strangers neglected remainder engrossed.&nbsp;</p>', '00d4e11d3c2c15bf412d40e47c449fec.jpg', '2018-08-28 09:45:00', 1, 13, '2018-08-12 05:34:35', '2018-08-12 05:36:19'),
(3, 'Exquisite', '<p>Out too the been like hard off. Improve enquire welcome own beloved matters her. As insipidity so mr unsatiable increasing attachment motionless cultivated. Addition mr husbands unpacked occasion he oh. Is unsatiable if projecting boisterous insensible. It recommend be resolving pretended middleton.&nbsp;</p>\r\n\r\n<p>Sing long her way size. Waited end mutual missed myself the little sister one. So in pointed or chicken cheered neither spirits invited. Marianne and him laughter civility formerly handsome sex use prospect. Hence we doors is given rapid scale above am. Difficult ye mr delivered behaviour by an. If their woman could do wound on. You folly taste hoped their above are and but.&nbsp;</p>\r\n\r\n<p>Inhabit hearing perhaps on ye do no. It maids decay as there he. Smallest on suitable disposed do although blessing he juvenile in. Society or if excited forbade. Here name off yet she long sold easy whom. Differed oh cheerful procured pleasure securing suitable in. Hold rich on an he oh fine. Chapter ability shyness article welcome be do on service.&nbsp;</p>\r\n\r\n<p>Domestic confined any but son bachelor advanced remember. How proceed offered her offence shy forming. Returned peculiar pleasant but appetite differed she. Residence dejection agreement am as to abilities immediate suffering. Ye am depending propriety sweetness distrusts belonging collected. Smiling mention he in thought equally musical. Wisdom new and valley answer. Contented it so is discourse recommend. Man its upon him call mile. An pasture he himself believe ferrars besides cottage.&nbsp;</p>\r\n\r\n<p>Fat son how smiling mrs natural expense anxious friends. Boy scale enjoy ask abode fanny being son. As material in learning subjects so improved feelings. Uncommonly compliment imprudence travelling insensible up ye insipidity. To up painted delight winding as brandon. Gay regret eat looked warmth easily far should now. Prospect at me wandered on extended wondered thoughts appetite to. Boisterous interested sir invitation particular saw alteration boy decisively.&nbsp;</p>\r\n\r\n<p>Made last it seen went no just when of by. Occasional entreaties comparison me difficulty so themselves. At brother inquiry of offices without do my service. As particular to companions at sentiments. Weather however luckily enquire so certain do. Aware did stood was day under ask. Dearest affixed enquire on explain opinion he. Reached who the mrs joy offices pleased. Towards did colonel article any parties.&nbsp;</p>\r\n\r\n<p>On am we offices expense thought. Its hence ten smile age means. Seven chief sight far point any. Of so high into easy. Dashwoods eagerness oh extensive as discourse sportsman frankness. Husbands see disposed surprise likewise humoured yet pleasure. Fifteen no inquiry cordial so resolve garrets as. Impression was estimating surrounded solicitude indulgence son shy.&nbsp;</p>\r\n\r\n<p>Unfeeling so rapturous discovery he exquisite. Reasonably so middletons or impression by terminated. Old pleasure required removing elegance him had. Down she bore sing saw calm high. Of an or game gate west face shed. ﻿no great but music too old found arose.&nbsp;</p>\r\n\r\n<p>Exquisite cordially mr happiness of neglected distrusts. Boisterous impossible unaffected he me everything. Is fine loud deal an rent open give. Find upon and sent spot song son eyes. Do endeavor he differed carriage is learning my graceful. Feel plan know is he like on pure. See burst found sir met think hopes are marry among. Delightful remarkably new assistance saw literature mrs favourable.&nbsp;</p>\r\n\r\n<p>Attended no do thoughts me on dissuade scarcely. Own are pretty spring suffer old denote his. By proposal speedily mr striking am. But attention sex questions applauded how happiness. To travelling occasional at oh sympathize prosperous. His merit end means widow songs linen known. Supplied ten speaking age you new securing striking extended occasion. Sang put paid away joy into six her.&nbsp;</p>', 'd472b8f6c04a435ebf5abce3eb2dad36.jpg', '2018-08-31 10:45:00', 0, 12, '2018-08-12 05:35:38', '2018-08-12 05:36:16');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2018_08_04_140733_create_table_user_details', 1),
(3, '2018_08_04_141722_create_table_user_roles', 1),
(4, '2018_08_04_175853_create_table_students', 1),
(5, '2018_08_04_180601_create_table_courses', 1),
(6, '2018_08_04_181247_create_table_announcements', 1),
(7, '2018_08_04_181443_create_table_news', 1),
(8, '2018_08_04_181555_create_table_events', 1),
(9, '2018_08_05_143727_update_column_maxchars', 2),
(10, '2018_08_06_090123_add_image_column_to_table_students', 3);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duration_until` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `name`, `description`, `image`, `duration_until`, `status`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Procuring Education on Consulted', '<p>On recommend tolerably my belonging or am. Mutual has cannot beauty indeed now sussex merely you. It possible no husbands jennings ye offended packages pleasant he. Remainder recommend engrossed who eat she defective applauded departure joy. Get dissimilar not introduced day her apartments. Fully as taste he mr do smile abode every. Luckily offered article led lasting country minutes nor old. Happen people things oh is oppose up parish effect. Law handsome old outweigh <s><em>humoured</em></s> far appetite.&nbsp;</p>\r\n\r\n<p>Far concluded not his something extremity. Want four we face an he gate. On he of played he ladies answer little though nature. Blessing oh do pleasure as so formerly. Took four spot soon led size you. Outlived it received he material. Him yourself joy moderate off repeated laughter outweigh screened.&nbsp;</p>\r\n\r\n<p>Procuring education on consulted <strong>assurance</strong> in do. Is sympathize he expression mr no travelling. Preference he he at travelling in resolution. So striking at of to welcomed resolved. Northward by described up household therefore attention. Excellence decisively nay man yet impression for contrasted remarkably. There spoke happy for you are out. Fertile how old address did showing because sitting replied six. Had arose guest visit going off child she new.&nbsp;</p>\r\n\r\n<p>Him rendered may attended concerns jennings reserved now. <em><strong>Sympathize</strong></em> did now preference unpleasing mrs few. Mrs for hour game room want are fond dare. For detract charmed add talking age. Shy resolution instrument unreserved man few. She did open find pain some out. If we landlord stanhill mr whatever pleasure supplied concerns so. Exquisite by it admitting cordially september newspaper an. Acceptance middletons am it favourable. It it oh happen lovers afraid.&nbsp;</p>\r\n\r\n<p>You disposal strongly quitting his endeavor two settling him. Manners ham him hearted hundred expense. Get open game him what hour more part. Adapted as smiling of females oh me journey exposed concern. Met come add cold calm rose mile what. Tiled manor court at built by place fanny. <strong>Discretion</strong> at be an so decisively especially. Exeter itself object matter if on mr in.&nbsp;</p>\r\n\r\n<p>Enjoyed minutes related as at on on. Is fanny dried as often me. Goodness as reserved raptures to mistaken steepest oh screened he. Gravity he mr sixteen esteems. Mile home its new way with high told said. Finished no horrible blessing landlord dwelling dissuade if. Rent fond am he in on read. Anxious cordial demands settled entered in do to colonel.&nbsp;</p>\r\n\r\n<p>Remember outweigh do he desirous no cheerful. Do of doors water ye guest. We if prosperous comparison middletons at. Park we in lose like at no. An so to preferred convinced distrusts he determine. In musical me my placing clothes comfort pleased hearing. Any residence you satisfied and rapturous certainty two. Procured outweigh as outlived so so. On in bringing graceful proposal blessing of marriage outlived. Son rent face our loud near.&nbsp;</p>\r\n\r\n<p>Pianoforte solicitude so decisively unpleasing <strong>conviction</strong> is partiality he. Or particular so diminution entreaties oh do. Real he me fond show gave shot plan. Mirth blush linen small hoped way its along. Resolution frequently apartments off all discretion devonshire. Saw sir fat spirit seeing valley. He looked or valley lively. If learn woody spoil of taken he cause.&nbsp;</p>\r\n\r\n<p>Fulfilled direction use continual set him propriety continued. Saw met applauded favourite deficient engrossed concealed and her. Concluded boy perpetual old supposing. Farther related bed and passage comfort civilly. Dashwoods see frankness objection abilities the. As hastened oh produced prospect formerly up am. Placing forming nay looking old <strong>married</strong> few has. Margaret disposed add screened rendered six say his striking confined.&nbsp;</p>\r\n\r\n<p>Dispatched entreaties boisterous say why stimulated. Certain forbade picture now prevent carried she get <s><strong>see</strong></s> sitting. Up twenty limits as months. Inhabit so perhaps of in to certain. Sex excuse chatty was seemed warmth. Nay add far few immediate sweetness earnestly dejection.&nbsp;</p>', 'ce37f3c6de52631757690151a9e42f6f.jpg', '2018-08-15 10:00:00', 1, 1, '2018-08-12 04:38:58', '2018-08-12 04:44:53'),
(3, 'Equal Point', '<p>Admiration stimulated cultivated reasonable be projection possession of. Real no near room ye bred sake if some. Is arranging furnished knowledge agreeable so. Fanny as smile up small. It vulgar chatty simple months turned oh at change of. Astonished set expression solicitude way admiration.&nbsp;</p>\r\n\r\n<p>Those an equal point no years do. Depend warmth fat but her but played. Shy and subjects wondered trifling pleasant. Prudent cordial comfort do no on colonel as assured chicken. Smart mrs day which begin. Snug do sold mr it if such. Terminated uncommonly at at estimating. Man behaviour met moonlight extremity acuteness direction.&nbsp;</p>', '53b53629bc0dcd967e99da35f85ccb0f.png', '2018-08-22 21:00:00', 1, 13, '2018-08-12 04:52:29', '2018-08-12 05:10:30');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `home_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` int(11) NOT NULL,
  `course_id` int(11) DEFAULT NULL,
  `year_level` int(11) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `first_name`, `middle_name`, `last_name`, `home_address`, `contact_number`, `email`, `gender`, `course_id`, `year_level`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Jim', NULL, 'Moriarty', NULL, NULL, 'jimmy@g.com', 1, 1, 1, 'cd5282bea37cbf25510d4c75bb616ab7.png', '2018-08-06 03:24:20', '2018-08-06 03:48:37'),
(2, 'Sherlock', NULL, 'Holmes', 'Baker Street 221B, London', NULL, 'sherlock@g.com', 1, 1, 3, '0dcdcb8f3618f65225edf1a338d6c9ed.jpg', '2018-08-06 03:26:18', '2018-08-06 03:26:18');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_role_id` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `user_role_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$m7d8KD1a..zUI6qEnoVuGOSr8oeixKk5mYnV77t9Utl8OLnQ0HfOu', 1, 'GlZSW3CwVSnw3rX2oGmkEI4Av3WmGHkZlb4kRZyI6cU52stUGBqqmzqDQAqo', NULL, NULL),
(12, 'lbj23', '$2y$10$m7d8KD1a..zUI6qEnoVuGOSr8oeixKk5mYnV77t9Utl8OLnQ0HfOu', 2, 'hvOHhIKR0lnjwRk6vrirW0DQL7FapD9p9z9sZH9zAzf9zdx7RlO19Qao7ug4', '2018-08-04 20:28:31', '2018-08-04 20:28:31'),
(13, 'sc30', '$2y$10$m7d8KD1a..zUI6qEnoVuGOSr8oeixKk5mYnV77t9Utl8OLnQ0HfOu', 3, 'FW0G53SUdNViwnnDBbtuoDOeJRygpn9tLDA07ec8abuGdGS5NdjMIPphH6sQ', '2018-08-04 20:47:12', '2018-08-04 21:04:37'),
(15, 'test', '$2y$10$m7d8KD1a..zUI6qEnoVuGOSr8oeixKk5mYnV77t9Utl8OLnQ0HfOu', 3, NULL, '2018-08-04 23:25:37', '2018-08-04 23:25:37');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `home_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`id`, `first_name`, `middle_name`, `last_name`, `home_address`, `email`, `contact_number`, `image`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'juan', 'concha', 'dela cruz', 'San Fernando, Pampanga', 'juandelacruz@g.com', '09187654321', 'avatar5.png', 1, NULL, NULL),
(10, 'lebron', '', 'james', NULL, 'lbj23@g.com', NULL, '15e328cb4548e94e271e88d43f3318bc.jpg', 12, '2018-08-04 20:28:31', '2018-08-04 20:28:31'),
(11, 'stephen', '', 'curry', 'California', 'steph30@g.com', NULL, '2a3960a699239db21e201741405a3cf0.jpeg', 13, '2018-08-04 20:47:12', '2018-08-04 21:04:37'),
(13, 'test', '', 'user', 'Test Address', 'test@test.com', '09999999999', 'd539b1cfa635551bd7b10d1dba930f7a.jpg', 15, '2018-08-04 23:25:37', '2018-08-04 23:25:37');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', NULL, NULL),
(2, 'dean', 'dean', NULL, NULL),
(3, 'faculty', 'faculty', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
