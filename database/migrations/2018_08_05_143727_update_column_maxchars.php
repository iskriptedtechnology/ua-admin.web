<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnMaxchars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->text('description')->nullable()->change(); 
        });

        Schema::table('announcements', function (Blueprint $table) {
            $table->text('description')->nullable()->change(); 
        });

        Schema::table('news', function (Blueprint $table) {
            $table->text('description')->nullable()->change(); 
        });

        Schema::table('events', function (Blueprint $table) {
            $table->text('description')->nullable()->change(); 
        });

        Schema::table('user_roles', function (Blueprint $table) {
            $table->text('description')->nullable()->change(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->string('description')->nullable()->change(); 
        });

        Schema::table('announcements', function (Blueprint $table) {
            $table->string('description')->nullable()->change(); 
        });

        Schema::table('news', function (Blueprint $table) {
            $table->string('description')->nullable()->change(); 
        });

        Schema::table('events', function (Blueprint $table) {
            $table->string('description')->nullable()->change(); 
        });

        Schema::table('user_roles', function (Blueprint $table) {
            $table->string('description')->nullable()->change(); 
        });
    }
}
