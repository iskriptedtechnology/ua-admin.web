<?php

use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_roles')->insert([
            'name' => 'admin',
            'description' => 'admin'
        ]);

        DB::table('user_roles')->insert([
            'name' => 'dean',
            'description' => 'dean'
        ]);

        DB::table('user_roles')->insert([
            'name' => 'faculty',
            'description' => 'faculty'
        ]);
    }
}
