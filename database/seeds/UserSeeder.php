<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username'      => 'admin',
            'password'      => bcrypt('123456'),
            'user_role_id'  => 1
        ]);

        DB::table('user_details')->insert([
            'first_name'        => 'juan',
            'middle_name'       => 'concha',
            'last_name'         => 'dela cruz',
            'home_address'      => 'San Fernando, Pampanga',
            'email'             => 'juandelacruz@g.com',
            'contact_number'    => '09187654321',
            'image'             => 'avatar5.png',
            'user_id'           => 1,
        ]);
    }
}
