<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['middleware' => ['api']], function () {
    
    /**
     * Login
     */
    Route::post('/authenticate', 'Api\AuthController@authenticate');
    Route::post('/logout', 'Api\AuthController@logout');
    Route::get('/me', 'Api\AuthController@me');

    /**
     * Announcement
     */
    Route::group(['prefix' => 'announcement'], function () {
        
        Route::get('/all/get', 'Api\AnnouncementController@getAll')->name('announcement.all');
        Route::get('/detail/get/{id}', 'Api\AnnouncementController@getDetails')->name('announcement.get');
        Route::post('/upload/create', 'Api\AnnouncementController@create')->name('announcement.create');
        Route::post('/upload/update', 'Api\AnnouncementController@update')->name('announcement.update');
        Route::delete('/delete/{id}', 'Api\AnnouncementController@delete')->name('announcement.delete');
        Route::get('/check/{id}', 'Api\AnnouncementController@check')->name('announcement.check');
    });

    /**
     * News
     */
    Route::group(['prefix' => 'news'], function () {
        
        Route::get('/all/get', 'Api\NewsController@getAll')->name('news.all');
        Route::get('/detail/get/{id}', 'Api\NewsController@getDetails')->name('news.get');
        Route::post('/upload/create', 'Api\NewsController@create')->name('announcement.create');
        Route::post('/upload/update', 'Api\NewsController@update')->name('announcement.update');
        Route::delete('/delete/{id}', 'Api\NewsController@delete')->name('announcement.delete');
        Route::get('/check/{id}', 'Api\NewsController@check')->name('announcement.check');
    });

    /**
     * Events
     */
    Route::group(['prefix' => 'event'], function () {
        
        Route::get('/all/get', 'Api\EventController@getAll')->name('event.all');
        Route::get('/detail/get/{id}', 'Api\EventController@getDetails')->name('event.get');
        Route::post('/upload/create', 'Api\EventController@create')->name('announcement.create');
        Route::post('/upload/update', 'Api\EventController@update')->name('announcement.update');
        Route::delete('/delete/{id}', 'Api\EventController@delete')->name('announcement.delete');
        Route::get('/check/{id}', 'Api\EventController@check')->name('announcement.check');
    });

    /**
     * General
     */
    Route::get('/post/pending/count', 'Api\GeneralController@getPendingPostCounts')->name('general.pending_post_count.get');
});