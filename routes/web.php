<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Authentication Route
 */
Route::group(['middleware' => ['web']], function () {
    
    Route::get('/login', 'AuthController@login')->name('login');
    Route::post('/authenticate', 'AuthController@authenticate')->name('authenticate');
    Route::post('/logout', 'AuthController@logout')->name('logout');
});

/**
 * Auth Middleware Activated
 */
Route::group(['middleware' => ['auth', 'web']], function () {

    /**
     * Home Route
     */
    Route::get('/', 'HomeController@index')->name('home.index');

    /**
     * Announcement Route
     */
    Route::group(['prefix' => 'announcements'], function () {

        Route::get('/', 'AnnouncementController@index')->name('announcement.index');
        Route::get('/view/{announcement_id}', 'AnnouncementController@view')->name('announcement.view');
        Route::get('/create', 'AnnouncementController@create')->name('announcement.create');
        Route::post('/create', 'AnnouncementController@store')->name('announcement.store');
        Route::get('/update/{announcement_id}', 'AnnouncementController@edit')->name('announcement.edit');
        Route::post('/update', 'AnnouncementController@update')->name('announcement.update');
        Route::get('/delete/{announcement_id}', 'AnnouncementController@delete')->name('announcement.delete');
        Route::delete('/delete/{announcement_id}', 'AnnouncementController@remove')->name('announcement.remove');
        Route::get('/approve/{announcement_id}', 'AnnouncementController@approve')->name('announcement.approve');
        Route::get('/archive/{announcement_id}', 'AnnouncementController@archive')->name('announcement.archive');
    });

    /**
     * News Route
     */
    Route::group(['prefix' => 'news'], function () {

        Route::get('/', 'NewsController@index')->name('news.index');
        Route::get('/view/{news_id}', 'NewsController@view')->name('news.view');
        Route::get('/create', 'NewsController@create')->name('news.create');
        Route::post('/create', 'NewsController@store')->name('news.store');
        Route::get('/update/{news_id}', 'NewsController@edit')->name('news.edit');
        Route::post('/update', 'NewsController@update')->name('news.update');
        Route::get('/delete/{news_id}', 'NewsController@delete')->name('news.delete');
        Route::delete('/delete/{news_id}', 'NewsController@remove')->name('news.remove');
        Route::get('/approve/{news_id}', 'NewsController@approve')->name('news.approve');
        Route::get('/archive/{news_id}', 'NewsController@archive')->name('news.archive');
    });

    /**
     * Event Route
     */
    Route::group(['prefix' => 'events'], function () {

        Route::get('/', 'EventController@index')->name('event.index');
        Route::get('/view/{event_id}', 'EventController@view')->name('event.view');
        Route::get('/create', 'EventController@create')->name('event.create');
        Route::post('/create', 'EventController@store')->name('event.store');
        Route::get('/update/{event_id}', 'EventController@edit')->name('event.edit');
        Route::post('/update', 'EventController@update')->name('event.update');
        Route::get('/delete/{event_id}', 'EventController@delete')->name('event.delete');
        Route::delete('/delete/{event_id}', 'EventController@remove')->name('event.remove');
        Route::get('/approve/{event_id}', 'EventController@approve')->name('event.approve');
        Route::get('/archive/{event_id}', 'EventController@archive')->name('event.archive');
    });

    /**
     * Course Route
     */
    Route::group(['prefix' => 'courses'], function () {

        Route::get('/', 'CourseController@index')->name('course.index');
        Route::get('/create', 'CourseController@create')->name('course.create');
        Route::post('/create', 'CourseController@store')->name('course.store');
        Route::get('/update/{course_id}', 'CourseController@edit')->name('course.edit');
        Route::patch('/update', 'CourseController@update')->name('course.update');
        Route::get('/delete/{course_id}', 'CourseController@delete')->name('course.delete');
        Route::delete('/delete/{course_id}', 'CourseController@remove')->name('course.remove');
    });

    /**
     * Student Route
     */
    Route::group(['prefix' => 'students'], function () {

        Route::get('/', 'StudentController@index')->name('student.index');
        Route::get('/create', 'StudentController@create')->name('student.create');
        Route::post('/create', 'StudentController@store')->name('student.store');
        Route::get('/update/{student_id}', 'StudentController@edit')->name('student.edit');
        Route::post('/update', 'StudentController@update')->name('student.update');
        Route::get('/delete/{student_id}', 'StudentController@delete')->name('student.delete');
        Route::delete('/delete/{student_id}', 'StudentController@remove')->name('student.remove');
        Route::get('/year/get/{course_id}', 'StudentController@getYearLevels')->name('student.year_level.get');
    });

    /**
     * User Route
     */
    Route::group(['prefix' => 'users'], function () {

        Route::get('/', 'UserController@index')->name('user.index');
        Route::get('/create', 'UserController@create')->name('user.create');
        Route::post('/create', 'UserController@store')->name('user.store');
        Route::get('/update/{user_id}', 'UserController@edit')->name('user.edit');
        Route::post('/update', 'UserController@update')->name('user.update');
        Route::get('/delete/{user_id}', 'UserController@delete')->name('user.delete');
        Route::delete('/delete/{user_id}', 'UserController@remove')->name('user.remove');
    });


});