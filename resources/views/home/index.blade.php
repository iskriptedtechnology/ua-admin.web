@extends('layouts.master', ['user' => $user])

@section('title', 'Dashboard')

@section('styles')
<style>
.users-list>li img {
    min-height: 93px;
}
</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Version 1.0</small>
        </h1>
        {{-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol> --}}
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-bullhorn"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Announcements</span>
                        <span class="info-box-number">{{ number_format($announcements_count) }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-address-book-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">News</span>
                        <span class="info-box-number">{{ number_format($news_count) }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-calendar"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Events</span>
                        <span class="info-box-number">{{ number_format($events_count) }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-user"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Students</span>
                        <span class="info-box-number">{{ number_format($students_count) }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-md-12">
                <div class="row">

                    <!-- Announcements -->
                    <div class="col-md-4">
                        <!-- DIRECT CHAT -->
                        <div class="box box-primary direct-chat direct-chat-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Recent Announcements</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                
                                <!-- Conversations are loaded here -->
                                <div class="direct-chat-messages">
                                    
                                    @if ($announcements->count() > 0)
                                    @foreach ($announcements as $announcement)
                                    <!-- Message. Default to the left -->
                                    <div class="direct-chat-msg">
                                        <div class="direct-chat-info clearfix">
                                            <span class="direct-chat-name pull-left">{{ ucwords($announcement->user->detail->first_name.' '.$announcement->user->detail->last_name) }}</span>
                                            <span class="direct-chat-timestamp pull-right">{{ $announcement->date }}</span>
                                        </div>
                                        <!-- /.direct-chat-info -->
                                        <img class="direct-chat-img" src="{{ url('assets/img/'.$announcement->user->detail->image) }}" alt="message user image">
                                        <!-- /.direct-chat-img -->
                                        <div class="direct-chat-text">
                                            {{ $announcement->name }}
                                        </div>
                                        <!-- /.direct-chat-text -->
                                    </div>
                                    <!-- /.direct-chat-msg -->
                                    @endforeach
                                    @endif

                                </div>
                                <!--/.direct-chat-messages-->
                                
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                    <!-- /.col -->

                    <!-- News -->
                    <div class="col-md-4">
                        <!-- DIRECT CHAT -->
                        <div class="box box-primary direct-chat direct-chat-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Recent News</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <!-- Conversations are loaded here -->
                                <div class="direct-chat-messages">
                                    
                                    @if ($newss->count() > 0)
                                    @foreach ($newss as $news)
                                    <!-- Message. Default to the left -->
                                    <div class="direct-chat-msg">
                                        <div class="direct-chat-info clearfix">
                                            <span class="direct-chat-name pull-left">{{ ucwords($news->user->detail->first_name.' '.$news->user->detail->last_name) }}</span>
                                            <span class="direct-chat-timestamp pull-right">{{ $news->duration_until }}</span>
                                        </div>
                                        <!-- /.direct-chat-info -->
                                        <img class="direct-chat-img" src="{{ url('assets/img/'.$news->user->detail->image) }}" alt="message user image">
                                        <!-- /.direct-chat-img -->
                                        <div class="direct-chat-text">
                                            {{ $news->name }}
                                        </div>
                                        <!-- /.direct-chat-text -->
                                    </div>
                                    <!-- /.direct-chat-msg -->
                                    @endforeach
                                    @endif

                                </div>
                                <!--/.direct-chat-messages-->

                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                    <!-- /.col -->

                    <!-- Events -->
                    <div class="col-md-4">
                        <!-- DIRECT CHAT -->
                        <div class="box box-primary direct-chat direct-chat-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Recent Events</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                
                                <div class="direct-chat-messages">

                                    @if ($events->count() > 0)
                                    @foreach ($events as $event)
                                    <!-- Message. Default to the left -->
                                    <div class="direct-chat-msg">
                                        <div class="direct-chat-info clearfix">
                                            <span class="direct-chat-name pull-left">{{ ucwords($event->user->detail->first_name.' '.$event->user->detail->last_name) }}</span>
                                            <span class="direct-chat-timestamp pull-right">{{ $event->date }}</span>
                                        </div>
                                        <!-- /.direct-chat-info -->
                                        <img class="direct-chat-img" src="{{ url('assets/img/'.$event->user->detail->image) }}" alt="message user image">
                                        <!-- /.direct-chat-img -->
                                        <div class="direct-chat-text">
                                            {{ $event->name }}
                                        </div>
                                        <!-- /.direct-chat-text -->
                                    </div>
                                    <!-- /.direct-chat-msg -->
                                    @endforeach
                                    @endif

                                </div>

                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                    <!-- /.col -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.col -->

        </div>
        <!-- /.row -->

        {{-- <div class="row">
            <div class="col-md-12">
                
                <div class="row">

                    @if (Auth::user()->user_role_id == 1)
                    <div class="col-md-4">

                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Users</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body no-padding">
                                
                                @if ($admins->count() > 0)
                                <ul class="users-list clearfix">
                                    @foreach ($admins as $admin)
                                    <li>
                                        <img src="{{ url('/assets/img/'.$admin->detail->image) }}" alt="User Image">
                                        <a class="users-list-name" href="#">{{ ucwords($admin->detail->first_name.' '.$admin->detail->last_name) }}</a>
                                        <span class="users-list-date">{{ ucfirst($admin->role->name) }}</span>
                                    </li>
                                    @endforeach
                                </ul>
                                @endif

                                <!-- /.users-list -->
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer text-center">
                                <a href="{{ route('user.index') }}" class="uppercase">View All Users</a>
                            </div>
                            <!-- /.box-footer -->
                        </div>

                    </div>
                    @endif
                    
                </div>

            </div>
        </div> --}}

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection