@extends('layouts.login')

@section('content')

<div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    {!! Form::open(['route' => 'authenticate']) !!}
    <div class="form-group has-feedback">
        {!! Form::text('username', old('username'), ['class' => 'form-control', 'placeholder' => 'Username', 'required']) !!}
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password', 'required']) !!}
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
    </div>
    <div class="row">
        <div class="col-xs-8">
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
            {!! Form::submit('Sign In', ['class' => 'btn btn-primary btn-block btn-flat']) !!}
        </div>
        <!-- /.col -->
    </div>
    <br>
    @if (session('status'))
    <div class="row">
        <div class="col-xs-12">
            <div class="callout callout-warning">
                <p>{{ session('status') }}</p>
            </div>
        </div>
    </div>
    @endif
    {!! Form::close() !!}

</div>
<!-- /.login-box-body -->

@endsection