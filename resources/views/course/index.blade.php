@extends('layouts.master', ['user' => Auth::user()])

@section('title', 'Courses Management')

@section('styles')
<!-- DataTables -->
<link rel="stylesheet" href="{{ url('assets/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Course Management
            <small>Student Courses List</small>
        </h1>
        <ol class="breadcrumb">
            <li class="">
                <a href="{{ route('course.create') }}"><i class="fa fa-user-plus"></i> Add Course</a>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        {{-- <h3 class="box-title">Data Table With Full Features</h3> --}}
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="userstable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Years</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($courses as $course)
                                <tr>
                                    <td>{{ $course->name }}</td>
                                    <td>{{ str_limit($course->description, 100) }}</td>
                                    <td>{{ $course->years }}</td>
                                    <td align="center">
                                        <a href="{{ route('course.edit', ['course_id' => $course->id]) }}" class="table-command btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="Edit Course">&nbsp;<i class="fa fa-pencil"></i></a>
                                        <a href="{{ route('course.delete', ['course_id' => $course->id]) }}" class="table-command btn btn-xs btn-danger" data-toggle="tooltip" data-placement="top" title="Delete Course">&nbsp;<i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</div>

@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ url('assets/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('assets/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- page script -->
<script>
    $(document).ready(function() {
        $('#userstable').DataTable({
            'ordering': false
        });
    });
</script>
@endsection