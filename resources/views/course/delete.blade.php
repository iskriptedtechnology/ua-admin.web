@extends('layouts.master', ['user' => Auth::user()])

@section('title', 'Delete User')

@section('content')
    
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Course Management
            <small>Delete Course</small>
        </h1>
        <ol class="breadcrumb">
            <li class="">
                <a href="{{ route('user.index') }}"><i class="fa fa-user"></i> Show Courses Table</a>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Are you sure you want to delete this course?</h3>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <p><strong>{{ $course->name }}</strong></p>
                        @if ($course->description)
                        <p>{{ $course->description }}</p>
                        @endif
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="pull-right">
                            {!! Form::open(['url' => route('course.remove', ['course_id' => $course->id]), 'method' => 'delete']) !!}
                            {!! Form::submit('Okay, Delete this Course', ['class' => 'btn btn-danger']) !!}
                            <a href="{{ route('course.index') }}" class="btn btn-default"><i class="fa fa-trash"></i> Go Back to Lists</a>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
</div>

@endsection