@extends('layouts.master', ['user' => Auth::user()])

@section('title', 'Edit Course')

@section('content')
    
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Course Management
            <small>Edit Course</small>
        </h1>
        <ol class="breadcrumb">
            <li class="">
                <a href="{{ route('course.index') }}"><i class="fa fa-user"></i> Show Courses Table</a>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        {{-- <h3 class="box-title">Data Table With Full Features</h3> --}}
                    </div>
                    <!-- /.box-header -->

                    <!-- form start -->
                    {!! Form::model($course, ['route' => 'course.update', 'method' => 'patch']) !!}
                    {!! Form::hidden('id', old('id')) !!}
                    <div class="box-body">
                        <div class="form-group {{ $errors->first('name') ? 'has-error' : '' }}">
                            {!! Form::label('name', 'Name *', ['class' => 'control-label']) !!}
                            {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => 'Name', 'required']) !!}
                            <span class="help-block">{{ $errors->first('name') }}</span>
                        </div>
                        <div class="form-group">
                            {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
                            {!! Form::textarea('description', old('description'), ['class' => 'form-control', 'placeholder' => 'Description']) !!}
                            <span class="help-block">{{ $errors->first('description') }}</span>
                        </div>
                        <div class="form-group {{ $errors->first('years') ? 'has-error' : '' }}">
                            {!! Form::label('years', 'Years *', ['class' => 'control-label']) !!}
                            {!! Form::number('years', old('years'), ['class' => 'form-control', 'placeholder' => 'Years', 'required']) !!}
                            <span class="help-block">{{ $errors->first('years') }}</span>
                        </div>
                        @if (session('status'))
                        <div class="form-group {{ session('status') ? 'has-error' : '' }}">
                            <span class="help-block">{{ session('status') }}</span>
                        </div>
                        @endif
                    </div>
                    <!-- /.box-body -->
    
                    <div class="box-footer">
                        {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                        {!! Form::reset('Clear Entries', ['class' => 'btn btn-default']) !!}
                    </div>
                    {!! Form::close() !!}

                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</div>

@endsection