@extends('layouts.master', ['user' => Auth::user()])

@section('title', 'Student Management')

@section('styles')
<!-- DataTables -->
<link rel="stylesheet" href="{{ url('assets/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Student Management
            <small>Students List</small>
        </h1>
        <ol class="breadcrumb">
            <li class="">
                <a href="{{ route('student.create') }}"><i class="fa fa-user-plus"></i> Add Student</a>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        {{-- <h3 class="box-title">Data Table With Full Features</h3> --}}
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="userstable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Display Photo</th>
                                    <th>Student ID</th>
                                    <th>Name</th>
                                    <th>Home Address</th>
                                    <th>Contact Number</th>
                                    <th>Email</th>
                                    <th>Gender</th>
                                    <th>Course</th>
                                    <th>Year Level</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($students as $student)
                                <tr>
                                    <td width="100" align="center"><img src="{{ url('assets/img').'/'.$student->image }}" class="display-photo-table"></td>
                                    <td>{{ $student->user->username }}</td>
                                    <td>{{ ucwords($student->first_name).' '.ucwords($student->last_name) }}</td>
                                    <td>{{ $student->home_address ? $student->home_address : 'N/A' }}</td>
                                    <td>{{ $student->contact_number ? $student->contact_number : 'N/A' }}</td>
                                    <td>{{ $student->email }}</td>
                                    <td>{{ $student->gender === 1 ? 'Male' : 'Female' }}</td>
                                    <td>{{ ucwords($student->course->name) }}</td>
                                    <td>{{ $student->year_level }}</td>
                                    <td align="center">
                                        <a href="{{ route('student.edit', ['user_id' => $student->id]) }}" class="table-command btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="Edit Student">&nbsp;<i class="fa fa-pencil"></i></a>
                                        <a href="{{ route('student.delete', ['user_id' => $student->id]) }}" class="table-command btn btn-xs btn-danger" data-toggle="tooltip" data-placement="top" title="Delete Student">&nbsp;<i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</div>

@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ url('assets/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('assets/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- page script -->
<script>
    $(document).ready(function() {
        $('#userstable').DataTable({
            'ordering': false
        });
    });
</script>
@endsection