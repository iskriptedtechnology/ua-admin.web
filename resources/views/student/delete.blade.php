@extends('layouts.master', ['user' => Auth::user()])

@section('title', 'Delete User')

@section('content')
    
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Student Management
            <small>Delete Student</small>
        </h1>
        <ol class="breadcrumb">
            <li class="">
                <a href="{{ route('student.index') }}"><i class="fa fa-user"></i> Show Students Table</a>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        Are you sure you want to delete this student?
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">
                        
                    </div>

                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header bg-white">
                            <div class="widget-user-image">
                                <img class="img-circle" src="{{ url('assets/img').'/'.$student->image }}" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">{{ ucwords($student->first_name).' '.ucwords($student->middle_name).' '.ucwords($student->last_name) }}</h3>
                            <h5 class="widget-user-desc">Student</h5>
                        </div>
                        <div class="box-footer no-padding">
                            <ul class="nav nav-stacked">
                                <li><a href="javascript:void(0)">{{ $student->gender === 1 ? 'Male' : 'Female' }}</a></li>
                                <li><a href="javascript:void(0)">{{ ucwords($student->course->name) }}</a></li>
                                <li><a href="javascript:void(0)">Year {{ $student->year_level }}</a></li>
                                <li><a href="javascript:void(0)">{{ $student->email }}</a></li>
                                @if ($student->home_address)
                                <li><a href="javascript:void(0)">{{ $student->home_address }}</a></li>
                                @endif
                                @if ($student->contact_number)
                                <li><a href="javascript:void(0)">{{ $student->contact_number }}</a></li>
                                @endif
                            </ul>
                        </div>
                    </div>

                    <div class="box-footer">
                        <div class="pull-right">
                            {!! Form::open(['url' => route('student.remove', ['student_id' => $student->id]), 'method' => 'delete']) !!}
                            {!! Form::submit('Okay, Delete this Student', ['class' => 'btn btn-danger']) !!}
                            <a href="{{ route('student.index') }}" class="btn btn-default"><i class="fa fa-trash"></i> Go Back to Lists</a>
                            {!! Form::close() !!}
                        </div>
                    </div>

                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </section>
</div>

@endsection