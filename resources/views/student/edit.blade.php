@extends('layouts.master', ['user' => Auth::user()])

@section('title', 'Edit User')

@section('content')
    
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Student Management
            <small>Edit Student</small>
        </h1>
        <ol class="breadcrumb">
            <li class="">
                <a href="{{ route('student.index') }}"><i class="fa fa-user"></i> Show Students Table</a>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        {{-- <h3 class="box-title">Data Table With Full Features</h3> --}}
                    </div>
                    <!-- /.box-header -->

                    <!-- form start -->
                    {!! Form::model($student, ['route' => 'student.update', 'enctype' => 'multipart/form-data']) !!}
                    {!! Form::hidden('id', old('id')) !!}
                    <!-- form start -->
                    <div class="box-body">
                        <div class="form-group {{ $errors->first('first_name') ? 'has-error' : '' }}">
                            {!! Form::label('first_name', 'First Name *', ['class' => 'control-label']) !!}
                            {!! Form::text('first_name', old('first_name'), ['class' => 'form-control', 'placeholder' => 'First Name', 'required']) !!}
                            <span class="help-block">{{ $errors->first('first_name') }}</span>
                        </div>
                        <div class="form-group">
                            {!! Form::label('middle_name', 'Middle Name', ['class' => 'control-label']) !!}
                            {!! Form::text('middle_name', old('middle_name'), ['class' => 'form-control', 'placeholder' => 'Middle Name']) !!}
                        </div>
                        <div class="form-group {{ $errors->first('last_name') ? 'has-error' : '' }}">
                            {!! Form::label('last_name', 'Last Name *', ['class' => 'control-label']) !!}
                            {!! Form::text('last_name', old('last_name'), ['class' => 'form-control', 'placeholder' => 'Last Name', 'required']) !!}
                            <span class="help-block">{{ $errors->first('last_name') }}</span>
                        </div>
                        <div class="form-group">
                            {!! Form::label('home_address', 'Home Address', ['class' => 'control-label']) !!}
                            {!! Form::text('home_address', old('home_address'), ['class' => 'form-control', 'placeholder' => 'Home Address']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('contact_number', 'Contact Number', ['class' => 'control-label']) !!}
                            {!! Form::text('contact_number', old('contact_number'), ['class' => 'form-control', 'placeholder' => 'Contact Number']) !!}
                        </div>
                        <div class="form-group {{ $errors->first('email') ? 'has-error' : '' }}">
                            {!! Form::label('email', 'Email Address *', ['class' => 'control-label']) !!}
                            {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Email Address', 'required']) !!}
                            <span class="help-block">{{ $errors->first('email') }}</span>
                        </div>
                        <div class="form-group {{ $errors->first('gender') ? 'has-error' : '' }}">
                            {!! Form::label('gender', 'Gender *', ['class' => 'control-label']) !!}
                            {!! Form::select('gender', ['1' => 'Male', '2' => 'Female'], null, ['class' => 'form-control', 'placeholder' => 'Select Gender']) !!}
                            <span class="help-block">{{ $errors->first('gender') }}</span>
                        </div>
                        <div class="form-group {{ $errors->first('course_id') ? 'has-error' : '' }}">
                            {!! Form::label('course_id', 'Course *', ['class' => 'control-label']) !!}
                            {!! Form::select('course_id', $courses, null, ['class' => 'form-control', 'placeholder' => 'Select Course']) !!}
                            <span class="help-block">{{ $errors->first('course_id') }}</span>
                        </div>
                        <div class="form-group {{ $errors->first('year_level') ? 'has-error' : '' }}">
                            {!! Form::label('year_level', 'Course *', ['class' => 'control-label']) !!}
                            <select id="year_level" name="year_level" class="form-control">
                                <option value="">Select Year Level</option>
                                @for ($i=1; $i<=$years; $i++)
                                @if ($i === $student['year_level'])
                                <option value="{{ $i }}" selected>{{ $i }}</option>
                                @else
                                <option value="{{ $i }}">{{ $i }}</option>
                                @endif
                                @endfor
                            </select>
                            <span class="help-block">{{ $errors->first('year_level') }}</span>
                        </div>
                        <div class="form-group">
                            {!! Form::label('image', 'Display Photo', ['class' => 'control-label']) !!}
                            {!! Form::file('image', ['accept' => 'image/*', 'class' => 'form-control']) !!}
                        </div>
                        @if (session('status'))
                        <div class="form-group {{ session('status') ? 'has-error' : '' }}">
                            <span class="help-block">{{ session('status') }}</span>
                        </div>
                        @endif
                    </div>
                    <!-- /.box-body -->
    
                    <div class="box-footer">
                        {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                        {!! Form::reset('Clear Entries', ['class' => 'btn btn-default']) !!}
                    </div>
                    {!! Form::close() !!}

                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</div>

@endsection

@section('scripts')
<script>
    $(document).ready(function () {

        $('#course_id').change(function (e) {

            $.ajax({
                url: '{{ URL::to('/') }}/students/year/get/' + $(this).val(),
                method: 'get',
                success: function (obj) {

                    var years = JSON.parse(obj).years;

                    $('#year_level option').remove();

                    for (i = 1; i <= years; i++) {

                        $('#year_level').append('<option value="' + i + '">' + i + '</option>');
                    }
                }
            });
        });
    });
</script>
@endsection