@extends('layouts.master', ['user' => Auth::user()])

@section('title', 'Announcement Management')

@section('styles')
<!-- DataTables -->
<link rel="stylesheet" href="{{ url('assets/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Announcement Management
            <small>Announcements List</small>
        </h1>
        <ol class="breadcrumb">
            <li class="">
                <a href="{{ route('announcement.create') }}"><i class="fa fa-user-plus"></i> Add Announcement</a>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        {{-- <h3 class="box-title">Data Table With Full Features</h3> --}}
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="announcementstable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>Published By</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($announcements as $announcement)
                                <tr>
                                    <td width="200" align="center"><img src="{{ url('assets/img').'/'.$announcement->image }}" class="display-photo-table"></td>
                                    <td>{{ $announcement->name }}</td>
                                    <td>{{ $announcement->date }}</td>
                                    
                                    @if ($announcement->status === 0)
                                    <td>Archived</td>
                                    @elseif($announcement->status === 1)
                                    <td>Approved</td>
                                    @elseif($announcement->status === 2)
                                    <td>Draft / Not Yet Approved</td>
                                    @endif

                                    <td>{{ ucwords($announcement->user->detail->first_name).' '.ucwords($announcement->user->detail->last_name) }}</td>
                                    <td align="center">
                                        <a href="{{ route('announcement.view', ['announcement_id' => $announcement->id]) }}" class="table-command btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" title="View Announcement">&nbsp;<i class="fa fa-eye"></i></a>
                                        
                                        @if ($announcement->status == 2 && (Auth::user()->user_role_id == 1 || Auth::user()->user_role_id == 2))
                                        <a href="{{ route('announcement.approve', ['announement_id' => $announcement->id]) }}" class="table-command btn btn-xs btn-success" data-toggle="tooltip" data-placement="top" title="Approve Announcement">&nbsp;<i class="fa fa-check"></i></a>
                                        @elseif ($announcement->status == 1 && (Auth::user()->user_role_id == 1 || Auth::user()->user_role_id == 2))
                                        <a href="{{ route('announcement.archive', ['announement_id' => $announcement->id]) }}" class="table-command btn btn-xs btn-warning" data-toggle="tooltip" data-placement="top" title="Archive Announcement">&nbsp;<i class="fa fa-remove"></i></a>
                                        @endif

                                        @if ($announcement->user_id == Auth::user()->id)
                                        <a href="{{ route('announcement.edit', ['announcement_id' => $announcement->id]) }}" class="table-command btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="Edit Announcement">&nbsp;<i class="fa fa-pencil"></i></a>
                                        @endif

                                        @if (Auth::user()->user_role_id == 1 || Auth::user()->id == $announcement->user->id)
                                        <a href="{{ route('announcement.delete', ['announcement_id' => $announcement->id]) }}" class="table-command btn btn-xs btn-danger" data-toggle="tooltip" data-placement="top" title="Delete Announcement">&nbsp;<i class="fa fa-trash"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</div>

@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ url('assets/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('assets/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- page script -->
<script>
    $(document).ready(function() {
        $('#announcementstable').DataTable({
            'ordering': false
        });
    });
</script>
@endsection