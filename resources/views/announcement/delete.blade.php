@extends('layouts.master', ['user' => Auth::user()])

@section('title', 'Delete Announcement')

@section('content')
    
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Announcement Management
            <small>Delete Announcement</small>
        </h1>
        <ol class="breadcrumb">
            <li class="">
                <a href="{{ route('announcement.index') }}"><i class="fa fa-user"></i> Show Announcements Table</a>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        Are you sure you want to delete this announcement?
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">
            
                        <!-- Attachment -->
                        <div class="attachment-block clearfix">
                            <img class="attachment-img" src="{{ url('assets/img/'.$announcement->image) }}" alt="Attachment Image">
            
                            <div class="attachment-pushed">
                                <h4 class="attachment-heading"><a href="{{ route('announcement.view', ['announcement_id' => $announcement->id]) }}">{{ $announcement->name }}</a></h4>
            
                                <div class="attachment-text">
                                    Published By: {{ ucwords($announcement->user->detail->first_name.' '.$announcement->user->detail->last_name) }} from {{ $announcement->date }}
                                </div>
                                <!-- /.attachment-text -->
                            </div>
                            <!-- /.attachment-pushed -->
                        </div>
                        <!-- /.attachment-block -->
            
                    </div>

                    <div class="box-footer">
                        <div class="pull-right">
                            {!! Form::open(['url' => route('announcement.remove', ['announcement_id' => $announcement->id]), 'method' => 'delete']) !!}
                            {!! Form::submit('Okay, Delete this Announcement', ['class' => 'btn btn-danger']) !!}
                            <a href="{{ route('announcement.index') }}" class="btn btn-default"><i class="fa fa-trash"></i> Go Back to Lists</a>
                            {!! Form::close() !!}
                        </div>
                    </div>

                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </section>
</div>

@endsection