@extends('layouts.master', ['user' => Auth::user()])

@section('title', $announcement->name)

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Announcement Management
            <small>Announcement Details</small>
        </h1>
        <ol class="breadcrumb">
            <li class="">
                <a href="{{ route('announcement.index') }}"><i class="fa fa-user-plus"></i> Show Announcements Table</a>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-6">

                <div class="box box-widget">
                    <div class="box-header with-border">
                        <div class="user-block">
                            <img class="img-circle" src="{{ url('/assets/img/'.$announcement->user->detail->image) }}" alt="User Image">
                            <span class="username"><a href="#">{{ ucwords($announcement->user->detail->first_name).' '.ucwords($announcement->user->detail->last_name) }}</a></span>
                            @if ($announcement->status == 2)
                            <span class="description">Not Approved Yet / Draft - {{ $announcement->date }}</span>
                            @elseif ($announcement->status == 0)
                            <span class="description">Archived - {{ $announcement->date }}</span>
                            @else
                            <span class="description">Shared - {{ $announcement->date }}</span>
                            @endif
                        </div>
                        <!-- /.user-block -->
                        <div class="box-tools">
                            {{-- <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Mark as read"><i class="fa fa-circle-o"></i></button> --}}
                            {{-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button> --}}
                            {{-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> --}}
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <img class="img-responsive pad" src="{{ url('/assets/img/'.$announcement->image) }}" alt="Photo" width="100%">
                        <br>
                        {!! $announcement->description !!}
                        {{-- <button type="button" class="btn btn-default btn-xs"><i class="fa fa-share"></i> Share</button> --}}
                        {{-- <button type="button" class="btn btn-default btn-xs"><i class="fa fa-thumbs-o-up"></i> Like</button> --}}
                        {{-- <span class="pull-right text-muted">127 likes - 3 comments</span> --}}
                    </div>
                    <!-- /.box-body -->
                    <!-- /.box-footer -->
                    <div class="box-footer">
                        @if ($announcement->user->id == Auth::user()->id || Auth::user()->user_role_id == 1)
                        <a href="{{ route('announcement.edit', ['announcement_id' => $announcement->id]) }}" class="btn btn-sm btn-default">Edit Details</a>
                        @endif

                        @if ((Auth::user()->user_role_id == 1 || Auth::user()->user_role_id == 2) && $announcement->status == 1)
                        <a href="{{ route('announcement.archive', ['announcement_id' => $announcement->id]) }}" class="btn btn-sm btn-warning">Archive Announcement</a>
                        @elseif ((Auth::user()->user_role_id == 1 || Auth::user()->user_role_id == 2) && $announcement->status == 2)
                        <a href="{{ route('announcement.approve', ['announcement_id' => $announcement->id]) }}" class="btn btn-sm btn-success">Approve Announcement</a>
                        @endif
                        
                        @if (Auth::user()->user_role_id == 1 || Auth::user()->id == $announcement->user->id)
                        <a href="{{ route('announcement.delete', ['announcement_id' => $announcement->id]) }}" class="btn btn-sm btn-danger">Delete Announcement</a>
                        @endif
                    </div>
                    <!-- /.box-footer -->
                </div>

            </div>
        </div>
    </section>
    
</div>

@endsection