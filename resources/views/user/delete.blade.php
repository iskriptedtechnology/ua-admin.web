@extends('layouts.master', ['user' => Auth::user()])

@section('title', 'Delete User')

@section('content')
    
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            User Management
            <small>Delete User</small>
        </h1>
        <ol class="breadcrumb">
            <li class="">
                <a href="{{ route('user.index') }}"><i class="fa fa-user"></i> Show Users Table</a>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        Are you sure you want to delete this user?
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">
                        
                    </div>

                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header bg-white">
                            <div class="widget-user-image">
                                <img class="img-circle" src="{{ url('assets/img').'/'.$user->detail->image }}" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">{{ ucwords($user->detail->first_name).' '.ucwords($user->detail->middle_name).' '.ucwords($user->detail->last_name) }}</h3>
                            <h5 class="widget-user-desc">{{ ucfirst($user->role->name) }}</h5>
                        </div>
                        <div class="box-footer no-padding">
                            <ul class="nav nav-stacked">
                                <li><a href="javascript:void(0)">{{ $user->detail->email }}</a></li>
                                @if ($user->detail->home_address)
                                <li><a href="javascript:void(0)">{{ $user->detail->home_address }}</a></li>
                                @endif
                                @if ($user->detail->contact_number)
                                <li><a href="javascript:void(0)">{{ $user->detail->contact_number }}</a></li>
                                @endif
                            </ul>
                        </div>
                    </div>

                    <div class="box-footer">
                        <div class="pull-right">
                            {!! Form::open(['url' => route('user.remove', ['user_id' => $user->id]), 'method' => 'delete']) !!}
                            {!! Form::submit('Okay, Delete this User', ['class' => 'btn btn-danger']) !!}
                            <a href="{{ route('user.index') }}" class="btn btn-default"><i class="fa fa-trash"></i> Go Back to Lists</a>
                            {!! Form::close() !!}
                        </div>
                    </div>

                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </section>
</div>

@endsection