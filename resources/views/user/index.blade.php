@extends('layouts.master', ['user' => Auth::user()])

@section('title', 'User Management')

@section('styles')
<!-- DataTables -->
<link rel="stylesheet" href="{{ url('assets/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            User Management
            <small>System Users List</small>
        </h1>
        <ol class="breadcrumb">
            <li class="">
                <a href="{{ route('user.create') }}"><i class="fa fa-user-plus"></i> Add User</a>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        {{-- <h3 class="box-title">Data Table With Full Features</h3> --}}
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="userstable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Display Photo</th>
                                    <th>Username</th>
                                    <th>Role</th>
                                    <th>Name</th>
                                    <th>Home Address</th>
                                    <th>Email</th>
                                    <th>Contact Number</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                <tr>
                                    <td width="100" align="center"><img src="{{ url('assets/img').'/'.$user->detail->image }}" class="display-photo-table"></td>
                                    <td>{{ $user->username }}</td>
                                    <td>{{ ucfirst($user->role->name) }}</td>
                                    <td>{{ ucwords($user->detail->first_name).' '.ucwords($user->detail->last_name) }}</td>
                                    <td>{{ $user->detail->home_address ? $user->detail->home_address : 'N/A' }}</td>
                                    <td>{{ $user->detail->email }}</td>
                                    <td>{{ $user->detail->contact_number ? $user->detail->contact_number : 'N/A' }}</td>
                                    <td align="center">
                                        <a href="{{ route('user.edit', ['user_id' => $user->id]) }}" class="table-command btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="Edit User">&nbsp;<i class="fa fa-pencil"></i></a>
                                        <a href="{{ route('user.delete', ['user_id' => $user->id]) }}" class="table-command btn btn-xs btn-danger" data-toggle="tooltip" data-placement="top" title="Delete User">&nbsp;<i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</div>

@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ url('assets/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('assets/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- page script -->
<script>
    $(document).ready(function() {
        $('#userstable').DataTable({
            'ordering': false
        });
    });
</script>
@endsection