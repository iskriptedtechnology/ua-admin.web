@extends('layouts.master', ['user' => Auth::user()])

@section('title', 'Delete News')

@section('content')
    
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            News Management
            <small>Delete News</small>
        </h1>
        <ol class="breadcrumb">
            <li class="">
                <a href="{{ route('news.index') }}"><i class="fa fa-user"></i> Show News Table</a>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        Are you sure you want to delete this news?
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">
            
                        <!-- Attachment -->
                        <div class="attachment-block clearfix">
                            <img class="attachment-img" src="{{ url('assets/img/'.$news->image) }}" alt="Attachment Image">
            
                            <div class="attachment-pushed">
                                <h4 class="attachment-heading"><a href="{{ route('news.view', ['news_id' => $news->id]) }}">{{ $news->name }}</a></h4>
            
                                <div class="attachment-text">
                                    Published By: {{ ucwords($news->user->detail->first_name.' '.$news->user->detail->last_name) }} from {{ $news->duration_until }}
                                </div>
                                <!-- /.attachment-text -->
                            </div>
                            <!-- /.attachment-pushed -->
                        </div>
                        <!-- /.attachment-block -->
            
                    </div>

                    <div class="box-footer">
                        <div class="pull-right">
                            {!! Form::open(['url' => route('news.remove', ['news_id' => $news->id]), 'method' => 'delete']) !!}
                            {!! Form::submit('Okay, Delete this News', ['class' => 'btn btn-danger']) !!}
                            <a href="{{ route('news.index') }}" class="btn btn-default"><i class="fa fa-trash"></i> Go Back to Lists</a>
                            {!! Form::close() !!}
                        </div>
                    </div>

                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </section>
</div>

@endsection