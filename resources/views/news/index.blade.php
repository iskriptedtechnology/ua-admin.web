@extends('layouts.master', ['user' => Auth::user()])

@section('title', 'News Management')

@section('styles')
<!-- DataTables -->
<link rel="stylesheet" href="{{ url('assets/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            News Management
            <small>News List</small>
        </h1>
        <ol class="breadcrumb">
            <li class="">
                <a href="{{ route('news.create') }}"><i class="fa fa-user-plus"></i> Add News</a>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        {{-- <h3 class="box-title">Data Table With Full Features</h3> --}}
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="newstable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Duration Until</th>
                                    <th>Status</th>
                                    <th>Published By</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($newss as $news)
                                <tr>
                                    <td width="200" align="center"><img src="{{ url('assets/img').'/'.$news->image }}" class="display-photo-table"></td>
                                    <td>{{ $news->name }}</td>
                                    <td>{{ $news->duration_until }}</td>
                                    
                                    @if ($news->status === 0)
                                    <td>Archived</td>
                                    @elseif($news->status === 1)
                                    <td>Approved</td>
                                    @elseif($news->status === 2)
                                    <td>Draft / Not Yet Approved</td>
                                    @endif

                                    <td>{{ ucwords($news->user->detail->first_name).' '.ucwords($news->user->detail->last_name) }}</td>
                                    <td align="center">
                                        <a href="{{ route('news.view', ['news_id' => $news->id]) }}" class="table-command btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" title="View News">&nbsp;<i class="fa fa-eye"></i></a>
                                        
                                        @if ($news->status == 2 && (Auth::user()->user_role_id == 1 || Auth::user()->user_role_id == 2))
                                        <a href="{{ route('news.approve', ['news_id' => $news->id]) }}" class="table-command btn btn-xs btn-success" data-toggle="tooltip" data-placement="top" title="Approve News">&nbsp;<i class="fa fa-check"></i></a>
                                        @elseif ($news->status == 1 && (Auth::user()->user_role_id == 1 || Auth::user()->user_role_id == 2))
                                        <a href="{{ route('news.archive', ['news_id' => $news->id]) }}" class="table-command btn btn-xs btn-warning" data-toggle="tooltip" data-placement="top" title="Archive News">&nbsp;<i class="fa fa-remove"></i></a>
                                        @endif

                                        @if ($news->user_id == Auth::user()->id)
                                        <a href="{{ route('news.edit', ['news_id' => $news->id]) }}" class="table-command btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="Edit News">&nbsp;<i class="fa fa-pencil"></i></a>
                                        @endif

                                        @if (Auth::user()->user_role_id == 1 || Auth::user()->id == $news->user->id)
                                        <a href="{{ route('news.delete', ['news_id' => $news->id]) }}" class="table-command btn btn-xs btn-danger" data-toggle="tooltip" data-placement="top" title="Delete News">&nbsp;<i class="fa fa-trash"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</div>

@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ url('assets/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('assets/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- page script -->
<script>
    $(document).ready(function() {
        $('#newstable').DataTable({
            'ordering': false
        });
    });
</script>
@endsection