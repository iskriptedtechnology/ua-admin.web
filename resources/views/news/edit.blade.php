@extends('layouts.master', ['user' => Auth::user()])

@section('title', 'Edit News')

@section('styles')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ url('assets/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="{{ url('/assets/timepicker/bootstrap-timepicker.min.css') }}">
@endsection

@section('content')
    
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            News Management
            <small>Edit News</small>
        </h1>
        <ol class="breadcrumb">
            <li class="">
                <a href="{{ route('news.index') }}"><i class="fa fa-user"></i> Show News Table</a>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        {{-- <h3 class="box-title">Data Table With Full Features</h3> --}}
                    </div>
                    <!-- /.box-header -->

                    <!-- form start -->
                    {!! Form::model($news, ['route' => 'news.update', 'enctype' => 'multipart/form-data']) !!}
                    {!! Form::hidden('id', old('id')) !!}
                    <div class="box-body">
                        <div class="form-group {{ $errors->first('name') ? 'has-error' : '' }}">
                            {!! Form::label('name', 'Name *', ['class' => 'control-label']) !!}
                            {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => 'Name', 'required']) !!}
                            <span class="help-block">{{ $errors->first('name') }}</span>
                        </div>
                        <div class="form-group {{ $errors->first('description') ? 'has-error' : '' }}">
                            {!! Form::label('description', 'Description *', ['class' => 'control-label']) !!}
                            {!! Form::textarea('description', old('description'), ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
                            <span class="help-block">{{ $errors->first('description') }}</span>
                        </div>
                        <div class="form-group">
                            {!! Form::label('image', 'Featured Image', ['class' => 'control-label']) !!}
                            {!! Form::file('image', ['accept' => 'image/*', 'class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('duration_until', 'Duration Until Date *', ['class' => 'control-label']) !!}
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                {!! Form::text('duration_until', old('duration_until'), ['id' => 'datepicker', 'class' => 'form-control pull-right', 'placeholder' => 'Duration Until Date', 'required']) !!}
                            </div>
                            <span class="help-block">{{ $errors->first('date') }}</span>
                        </div>
                        <div class="form-group">
                            {!! Form::label('time', 'Time *', ['class' => 'control-label']) !!}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                                {!! Form::text('time', old('time'), ['id' => 'timepicker', 'class' => 'form-control timepicker pull-right', 'placeholder' => 'Time', 'required']) !!}
                            </div>
                        </div>
                        @if (session('status'))
                        <div class="form-group {{ session('status') ? 'has-error' : '' }}">
                            <span class="help-block">{{ session('status') }}</span>
                        </div>
                        @endif
                    </div>
                    <!-- /.box-body -->
    
                    <div class="box-footer">
                        {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                        {!! Form::reset('Clear Entries', ['class' => 'btn btn-default']) !!}
                    </div>
                    {!! Form::close() !!}

                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</div>

@endsection

@section('scripts')
<!-- CK Editor -->
<script src="{{ url('assets/ckeditor/ckeditor.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ url('assets/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- bootstrap time picker -->
<script src="{{ url('assets/timepicker/bootstrap-timepicker.min.js') }}"></script>

<script>
    $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('description');
    });

    //Datepicker
    $('#datepicker').datepicker({
        autoclose: true
    });

    //Timepicker
    $('.timepicker').timepicker({
        showInputs: false
    });
</script>
@endsection