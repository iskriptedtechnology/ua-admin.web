@extends('layouts.master', ['user' => Auth::user()])

@section('title', $news->name)

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            News Management
            <small>News Details</small>
        </h1>
        <ol class="breadcrumb">
            <li class="">
                <a href="{{ route('news.index') }}"><i class="fa fa-user-plus"></i> Show News Table</a>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-6">

                <div class="box box-widget">
                    <div class="box-header with-border">
                        <div class="user-block">
                            <img class="img-circle" src="{{ url('/assets/img/'.$news->user->detail->image) }}" alt="User Image">
                            <span class="username"><a href="#">{{ ucwords($news->user->detail->first_name).' '.ucwords($news->user->detail->last_name) }}</a></span>
                            @if ($news->status == 2)
                            <span class="description">Not Approved Yet / Draft - {{ $news->duration_until }}</span>
                            @elseif ($news->status == 0)
                            <span class="description">Archived - {{ $news->duration_until }}</span>
                            @else
                            <span class="description">Shared - {{ $news->duration_until }}</span>
                            @endif
                        </div>
                        <!-- /.user-block -->
                        <div class="box-tools">
                            {{-- <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Mark as read"><i class="fa fa-circle-o"></i></button> --}}
                            {{-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button> --}}
                            {{-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> --}}
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <img class="img-responsive pad" src="{{ url('/assets/img/'.$news->image) }}" alt="Photo" width="100%">
                        <br>
                        <b>{{ $news->name }}</b>
                        <br>
                        <br>
                        {!! $news->description !!}
                        {{-- <button type="button" class="btn btn-default btn-xs"><i class="fa fa-share"></i> Share</button> --}}
                        {{-- <button type="button" class="btn btn-default btn-xs"><i class="fa fa-thumbs-o-up"></i> Like</button> --}}
                        {{-- <span class="pull-right text-muted">127 likes - 3 comments</span> --}}
                    </div>
                    <!-- /.box-body -->
                    <!-- /.box-footer -->
                    <div class="box-footer">
                        @if ($news->user->id == Auth::user()->id || Auth::user()->user_role_id == 1)
                        <a href="{{ route('news.edit', ['news_id' => $news->id]) }}" class="btn btn-sm btn-default">Edit Details</a>
                        @endif

                        @if ((Auth::user()->user_role_id == 1 || Auth::user()->user_role_id == 2) && $news->status == 1)
                        <a href="{{ route('news.archive', ['news_id' => $news->id]) }}" class="btn btn-sm btn-warning">Archive Announcement</a>
                        @elseif ((Auth::user()->user_role_id == 1 || Auth::user()->user_role_id == 2) && $news->status == 2)
                        <a href="{{ route('news.approve', ['news_id' => $news->id]) }}" class="btn btn-sm btn-success">Approve Announcement</a>
                        @endif
                        
                        @if (Auth::user()->user_role_id == 1 || Auth::user()->id == $news->user->id)
                        <a href="{{ route('news.delete', ['news_id' => $news->id]) }}" class="btn btn-sm btn-danger">Delete Announcement</a>
                        @endif
                    </div>
                    <!-- /.box-footer -->
                </div>

            </div>
        </div>
    </section>
    
</div>

@endsection