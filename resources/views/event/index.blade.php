@extends('layouts.master', ['user' => Auth::user()])

@section('title', 'Event Management')

@section('styles')
<!-- DataTables -->
<link rel="stylesheet" href="{{ url('assets/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Event Management
            <small>Events List</small>
        </h1>
        <ol class="breadcrumb">
            <li class="">
                <a href="{{ route('event.create') }}"><i class="fa fa-user-plus"></i> Add Event</a>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        {{-- <h3 class="box-title">Data Table With Full Features</h3> --}}
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="eventstable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>Published By</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($events as $event)
                                <tr>
                                    <td width="200" align="center"><img src="{{ url('assets/img').'/'.$event->image }}" class="display-photo-table"></td>
                                    <td>{{ $event->name }}</td>
                                    <td>{{ $event->date }}</td>
                                    
                                    @if ($event->status === 0)
                                    <td>Archived</td>
                                    @elseif($event->status === 1)
                                    <td>Approved</td>
                                    @elseif($event->status === 2)
                                    <td>Draft / Not Yet Approved</td>
                                    @endif

                                    <td>{{ ucwords($event->user->detail->first_name).' '.ucwords($event->user->detail->last_name) }}</td>
                                    <td align="center">
                                        <a href="{{ route('event.view', ['event_id' => $event->id]) }}" class="table-command btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" title="View Event">&nbsp;<i class="fa fa-eye"></i></a>
                                        
                                        @if ($event->status == 2 && (Auth::user()->user_role_id == 1 || Auth::user()->user_role_id == 2))
                                        <a href="{{ route('event.approve', ['event_id' => $event->id]) }}" class="table-command btn btn-xs btn-success" data-toggle="tooltip" data-placement="top" title="Approve Event">&nbsp;<i class="fa fa-check"></i></a>
                                        @elseif ($event->status == 1 && (Auth::user()->user_role_id == 1 || Auth::user()->user_role_id == 2))
                                        <a href="{{ route('event.archive', ['event_id' => $event->id]) }}" class="table-command btn btn-xs btn-warning" data-toggle="tooltip" data-placement="top" title="Archive Event">&nbsp;<i class="fa fa-remove"></i></a>
                                        @endif

                                        @if ($event->user_id == Auth::user()->id)
                                        <a href="{{ route('event.edit', ['event_id' => $event->id]) }}" class="table-command btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="Edit Event">&nbsp;<i class="fa fa-pencil"></i></a>
                                        @endif

                                        @if (Auth::user()->user_role_id == 1 || Auth::user()->id == $event->user->id)
                                        <a href="{{ route('event.delete', ['event_id' => $event->id]) }}" class="table-command btn btn-xs btn-danger" data-toggle="tooltip" data-placement="top" title="Delete Event">&nbsp;<i class="fa fa-trash"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</div>

@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ url('assets/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('assets/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- page script -->
<script>
    $(document).ready(function() {
        $('#eventstable').DataTable({
            'ordering': false
        });
    });
</script>
@endsection