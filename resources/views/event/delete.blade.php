@extends('layouts.master', ['user' => Auth::user()])

@section('title', 'Delete Event')

@section('content')
    
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Event Management
            <small>Delete Event</small>
        </h1>
        <ol class="breadcrumb">
            <li class="">
                <a href="{{ route('user.index') }}"><i class="fa fa-user"></i> Show Events Table</a>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        Are you sure you want to delete this event?
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">
            
                        <!-- Attachment -->
                        <div class="attachment-block clearfix">
                            <img class="attachment-img" src="{{ url('assets/img/'.$event->image) }}" alt="Attachment Image">
            
                            <div class="attachment-pushed">
                                <h4 class="attachment-heading"><a href="{{ route('event.view', ['announcement_id' => $event->id]) }}">{{ $event->name }}</a></h4>
            
                                <div class="attachment-text">
                                    Published By: {{ ucwords($event->user->detail->first_name.' '.$event->user->detail->last_name) }} from {{ $event->date }}
                                </div>
                                <!-- /.attachment-text -->
                            </div>
                            <!-- /.attachment-pushed -->
                        </div>
                        <!-- /.attachment-block -->
            
                    </div>

                    <div class="box-footer">
                        <div class="pull-right">
                            {!! Form::open(['url' => route('event.remove', ['event_id' => $event->id]), 'method' => 'delete']) !!}
                            {!! Form::submit('Okay, Delete this Event', ['class' => 'btn btn-danger']) !!}
                            <a href="{{ route('event.index') }}" class="btn btn-default"><i class="fa fa-trash"></i> Go Back to Lists</a>
                            {!! Form::close() !!}
                        </div>
                    </div>

                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </section>
</div>

@endsection