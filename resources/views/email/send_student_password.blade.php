<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Student Registration</title>
</head>
<body>
    
    <div>
        <p>Welcome, {{ ucwords($student->first_name.' '.$student->last_name) }}</p>
        <br>
        <p>To login into our Android App, please use the credentials below:</p>
        <p>
            Username: <b>{{ $student->username }}</b>
            <br>
            Password: <b>{{ $student->password_raw }}</b>
        </p>
        <br>
        <p>You may also change your password by going to the Android App's Settings > Change Password</p>
        <br>
        <br>
        <br>
        <p>If you don't have the app yet, you click this <a href="#">link</a> to download it.</p>
        <br>
        <br>
        <br>
        <p>
            Regards,
            <br>
            University of Assumptions
        </p>
    </div>

</body>
</html>