<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>User Registration</title>
</head>
<body>
    
    <div>
        <p>Welcome, {{ ucwords($user->detail->first_name.' '.$user->detail->last_name) }}</p>
        <br>
        <p>To login into our Android App, please use the credentials below:</p>
        <p>
            Username: <b>{{ $user->username }}</b>
            <br>
            Password: <b>{{ $user->password_raw }}</b>
        </p>
        <br>
        <p>You may also change your password by going to the Android App's Settings > Change Password</p>
        <br>
        <br>
        <br>
        <p>If you don't have the app yet, you click this <a href="#">link</a> to download it.</p>
        <br>
        <br>
        <br>
        <p>
            Regards,
            <br>
            University of Assumptions
        </p>
    </div>

</body>
</html>