<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="{{ Request::is('/') ? 'active' : '' }}">
                <a href="{{ route('home.index') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="header">INFORMATION</li>
            <li class="{{ Request::is('announcements') || Request::is('announcements/*') ? 'active' : '' }}">
                <a href="{{ route('announcement.index') }}">
                    <i class="fa fa-bullhorn"></i>
                    <span>Announcement</span>
                </a>
            </li>
            <li class="{{ Request::is('news') || Request::is('news/*') ? 'active' : '' }}">
                <a href="{{ route('news.index') }}">
                    <i class="fa fa-address-book-o"></i>
                    <span>News</span>
                </a>
            </li>
            <li class="{{ Request::is('events') || Request::is('events/*') ? 'active' : '' }}">
                <a href="{{ route('event.index') }}">
                    <i class="fa fa-calendar-o"></i>
                    <span>Events</span>
                </a>
            </li>
            @if ($user->user_role_id == 1 || $user->user_role_id == 2)
            <li class="header">STUDENTS</li>
            <li class="{{ Request::is('students') || Request::is('students/*') ? 'active' : '' }}">
                <a href="{{ route('student.index') }}">
                    <i class="fa fa-user"></i>
                    <span>Student Management</span>
                </a>
            </li>
            @endif
            @if ($user->user_role_id == 1 || $user->user_role_id == 2)
            <li class="header">COURSE</li>
            <li class="{{ Request::is('courses') || Request::is('courses/*') ? 'active' : '' }}">
                <a href="{{ route('course.index') }}">
                    <i class="fa fa-user"></i>
                    <span>Course Management</span>
                </a>
            </li>
            @endif
            @if ($user->user_role_id == 1)
            <li class="header">USERS</li>
            <li class="{{ Request::is('users') || Request::is('users/*') ? 'active' : '' }}">
                <a href="{{ route('user.index') }}">
                    <i class="fa fa-user-md"></i>
                    <span>User Management</span>
                </a>
            </li>
            @endif
        </ul>
    </section>
<!-- /.sidebar -->
</aside>