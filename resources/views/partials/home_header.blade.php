<header class="main-header">

    <!-- Logo -->
    <a href="{{ URL::to('/') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>U</b>A</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>UA</b> admin</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ url('assets/img').'/'.$user->detail->image }}" class="user-image" alt="User Image">
                        <span class="hidden-xs">{{ ucwords($user->detail->first_name).' '.ucwords($user->detail->last_name) }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{ url('assets/img').'/'.$user->detail->image }}" class="img-circle" alt="User Image">
                            <p>
                                {{ ucwords($user->detail->first_name).' '.ucwords($user->detail->last_name) }}
                                <small>{{ ucfirst($user->role->name) }}</small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                {!! Form::open(['route' => 'logout']) !!}
                                {!! Form::submit('Sign out', ['class' => 'btn btn-default btn-flat']) !!}
                                {!! Form::close() !!}
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>