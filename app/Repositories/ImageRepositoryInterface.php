<?php

namespace App\Repositories;

interface ImageRepositoryInterface
{
    /**
     * Generate Image
     * 
     */
    public function generateImage($new_filename, $tmp_path);

    /**
     * Process Image
     */
    public function processImage($request);
}