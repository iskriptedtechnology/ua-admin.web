<?php

namespace App\Repositories;

use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use App\Repositories\ImageRepositoryInterface;

class ImageRepository implements ImageRepositoryInterface
{
    /**
     * Generate Image
     * 
     */
    public function generateImage($new_filename, $tmp_path)
    {
        $save_path = public_path().'/assets/img/';

        if(!File::exists($save_path)) {

            File::makeDirectory($save_path, $mode = 0777, true, true);
        }

        // resize medium
        $img_resize = Image::make($tmp_path);
        $img_resize->resize(512, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $img_resize->save($save_path.$new_filename);
    }

    /**
     * Process Image
     */
    public function processImage($request)
    {
        $file           = $request->file('image');
        $filename       = $file->getClientOriginalName();
        $new_filename   = md5($filename . microtime()).'.'.$file->getClientOriginalExtension();

        return (object)[
            'file'          => $file,
            'new_filename'  => $new_filename   
        ];
    }
}
