<?php

namespace App\Http\Controllers;

use App\User;
use App\UserDetail;
use Illuminate\Http\Request;
use App\Mail\SendUserPassword;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Repositories\ImageRepositoryInterface;

class UserController extends Controller
{
    private $_imageRepo;

    /**
     * Constructor
     */
    public function __construct(ImageRepositoryInterface $imageRepo)
    {
        $this->_imageRepo = $imageRepo;
    }

    /**
     * GET
     * Get All Users
     * 
     */
    public function index()
    {
        $users = User::where([
            ['id', '!=', Auth::user()->id],
            ['user_role_id', '!=', 4]
        ])
        ->get();

        return view('user.index', compact('users'));
    }

    /**
     * GET
     * Show Create User UI
     * 
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * POST
     * Perform Create User
     * 
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username'      => 'required|unique:users',
            'email'         => 'required|email',
            'first_name'    => 'required',
            'last_name'     => 'required',
            'user_role_id'  => 'required'
        ]);

        if ($validator->fails()) {

            return redirect()->route('user.create')->withErrors($validator)->withInput();
        }

        $create = User::createUser($request);

        if ($create) {

            if ($request->file('image') != null) {

                $image = $this->_imageRepo->processImage($request);
                $this->_imageRepo->generateImage($image->new_filename, $image->file->getPathName());

                UserDetail::updateImage($create->id, $image->new_filename);

            } else {

                UserDetail::updateImage($create->id, 'avatar'.rand(1, 5).'.png');
            }

            Mail::to($create->detail->email)->send(new SendUserPassword($create));

            return redirect()->route('user.index');
        }

        return redirect()->route('user.create')->with('status', 'Unable to create user. Please try again.')->withInput();
    }

    /**
     * GET
     * Show Edit User UI
     * 
     */

    public function edit($user_id)
    {
        $data = User::find($user_id);

        $user = [
            'id'                => $data->id,
            'username'          => $data->username,
            'email'             => $data->detail->email,
            'first_name'        => ucwords($data->detail->first_name),
            'middle_name'       => ucwords($data->detail->middle_name),
            'last_name'         => ucwords($data->detail->last_name),
            'home_address'      => $data->detail->home_address,
            'contact_number'    => $data->detail->contact_number,
            'user_role_id'      => $data->user_role_id
        ];

        return view('user.edit', ['user' => $user]);
    }

    /**
     * POST
     * Perform Update User
     * 
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'         => 'required|email',
            'first_name'    => 'required',
            'last_name'     => 'required',
            'user_role_id'  => 'required'
        ]);

        if ($validator->fails()) {

            return redirect()->route('user.edit')->withErrors($validator)->withInput();
        }

        $update = User::updateUser($request);

        if ($update) {

            if ($request->file('image') != null) {

                $image = $this->_imageRepo->processImage($request);
                $this->_imageRepo->generateImage($image->new_filename, $image->file->getPathName());

                UserDetail::updateImage($update->id, $image->new_filename);
            }

            return redirect()->route('user.index');
        }

        return redirect()->route('user.edit')->with('status', 'Unable to update user. Please try again.')->withInput();
    }

    /**
     * GET
     * Display Delete User UI
     * 
     */
    public function delete($user_id)
    {
        $user = User::find($user_id);

        return view('user.delete', ['user' => $user]);
    }

    /**
     * DELETE
     * Delete User and User Details
     * 
     */
    public function remove($user_id)
    {
        $delete = User::where('id', $user_id)->delete();

        if ($delete) {

            UserDetail::where('user_id', $user_id)->delete();

            return redirect()->route('user.index');
        }
    }
}
