<?php

namespace App\Http\Controllers;

use App\Course;
use App\Student;
use Illuminate\Http\Request;
use App\Mail\SendStudentPassword;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Repositories\ImageRepositoryInterface;

class StudentController extends Controller
{
    private $_imageRepo;

    /**
     * Constructor
     */
    public function __construct(ImageRepositoryInterface $imageRepo)
    {
        $this->_imageRepo = $imageRepo;
    }

    /**
     * GET
     * Get All Students
     * 
     */
    public function index()
    {
        $students = Student::get();

        return view('student.index', compact('students'));
    }

    /**
     * GET
     * Show Create Student UI
     * 
     */
    public function create()
    {
        $courses = Course::pluck('name', 'id');

        return view('student.create', compact('courses'));
    }

    /**
     * POST
     * Perform Create Student
     * 
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id'       => 'required',
            'first_name'    => 'required',
            'last_name'     => 'required',
            'email'         => 'required',
            'gender'        => 'required',
            'course_id'     => 'required',
            'year_level'    => 'required'
        ]);

        if ($validator->fails()) {

            return redirect()->route('student.create')->withErrors($validator)->withInput();
        }

        $create = Student::createStudent($request);

        if ($create) {

            if ($request->file('image') != null) {

                $image = $this->_imageRepo->processImage($request);
                $this->_imageRepo->generateImage($image->new_filename, $image->file->getPathName());

                Student::updateImage($create->id, $image->new_filename);

            } else {

                Student::updateImage($create->id, 'avatar'.rand(1, 5).'.png');
            }

            Mail::to($create->email)->send(new SendStudentPassword($create));

            return redirect()->route('student.index');
        }

        return redirect()->route('student.create')->with('status', 'Unable to create student. Please try again.')->withInput();
    }

    /**
     * GET
     * Show Edit Student UI
     * 
     */

    public function edit($user_id)
    {
        $data = Student::find($user_id);
        $courses = Course::pluck('name', 'id');

        $student = [
            'id'                => $data->id,
            'first_name'        => ucwords($data->first_name),
            'middle_name'       => ucwords($data->middle_name),
            'last_name'         => ucwords($data->last_name),
            'home_address'      => (string)$data->home_address,
            'contact_number'    => (string)$data->contact_number,
            'email'             => $data->email,
            'gender'            => (int)$data->gender,
            'course_id'         => (int)$data->course->id,
            'year_level'        => (int)$data->year_level
        ];

        return view('student.edit', [
            'student'   => $student, 
            'courses'   => $courses,
            'years'     => $data->course->years
        ]);
    }

    /**
     * POST
     * Perform Update student
     * 
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name'    => 'required',
            'last_name'     => 'required',
            'email'         => 'required',
            'gender'        => 'required',
            'course_id'     => 'required',
            'year_level'    => 'required'
        ]);

        if ($validator->fails()) {

            return redirect()->route('student.edit', ['student_id' => $request->id])->withErrors($validator)->withInput();
        }

        $update = Student::updateStudent($request);

        if ($update) {

            if ($request->file('image') != null) {

                $image = $this->_imageRepo->processImage($request);
                $this->_imageRepo->generateImage($image->new_filename, $image->file->getPathName());

                Student::updateImage($update->id, $image->new_filename);
            }

            return redirect()->route('student.index');
        }

        return redirect()->route('student.edit')->with('status', 'Unable to update student. Please try again.')->withInput();
    }

    /**
     * GET
     * Display Delete Student UI
     * 
     */
    public function delete($student_id)
    {
        $student = Student::find($student_id);

        return view('student.delete', ['student' => $student]);
    }

    /**
     * DELETE
     * Delete User and User Details
     * 
     */
    public function remove($student_id)
    {
        $delete = Student::where('id', $student_id)->delete();

        return redirect()->route('student.index');
    }

    /**
     * GET
     * Get Year Levels Based On Course
     * 
     */
    public function getYearLevels($course_id)
    {
        $course = Course::find($course_id);

        return json_encode(['years' => (int)$course->years]);
    }
}
