<?php

namespace App\Http\Controllers;

use App\News;
use App\User;
use App\Event;
use App\Student;
use App\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
        $user                   = Auth::user();
        $students_count         = Student::count();
        $announcements_count    = Announcement::count();
        $news_count             = News::count();
        $events_count           = Event::count();

        $announcements = Announcement::where([
            'status' => 1
        ])
        ->orderBy('date', 'desc')
        ->get();

        $newss = News::where([
            'status' => 1
        ])
        ->orderBy('created_at', 'desc')
        ->get();

        $events = Event::where([
            'status' => 1
        ])
        ->orderBy('date', 'desc')
        ->get();

        $admins = User::where('id', '!=', Auth::user()->id)->take(8)->get();

        return view('home.index', [
            'user'                  => $user,
            'students_count'        => $students_count,
            'announcements_count'   => $announcements_count,
            'news_count'            => $news_count,
            'events_count'          => $events_count,
            'announcements'         => $announcements,
            'newss'                 => $newss,
            'events'                => $events,
            'admins'                => $admins
        ]);
    }
}
