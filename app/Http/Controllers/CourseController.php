<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CourseController extends Controller
{
    /**
     * GET
     * Get All Courses
     * 
     */
    public function index()
    {
        $courses = Course::get();

        return view('course.index', compact('courses'));
    }

    /**
     * GET
     * Display Create Course UI
     * 
     */
    public function create()
    {
        return view('course.create');
    }

    /**
     * POST
     * Perform Create Course
     * 
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required',
            'years' => 'required'
        ]);

        if ($validator->fails()) {

            return redirect()->route('course.create')->withErrors($validator)->withInput();
        }

        $create = Course::createCourse($request);

        return $create ? 
            redirect()->route('course.index') :
            redirect()->route('course.create')->withInput()->with('status', 'Unable to create course. Please try again.');
    }

    /**
     * GET
     * Display Edit Course UI
     * 
     */
    public function edit($course_id)
    {
        $course = Course::find($course_id);

        return view('course.edit', compact('course'));
    }

    /**
     * PATCH
     * Perform Update Course
     * 
     */
    public function update(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'name'  => 'required',
            'years' => 'required'
        ]);

        if ($validator->fails()) {

            return redirect()->route('course.create')->withErrors($validator)->withInput();
        }

        $update = Course::updatecourse($request);

        return $update ? 
            redirect()->route('course.index') :
            redirect()->route('course.edit')->withInput()->with('status', 'Unable to update course. Please try again.');
    }

    /**
     * GET
     * Display Delete User UI
     * 
     */
    public function delete($course_id)
    {
        $course = Course::find($course_id);

        return view('course.delete', ['course' => $course]);
    }

    /**
     * DELETE
     * Perform Delete Course
     * 
     */
    public function remove($course_id)
    {
        $delete = Course::where('id', $course_id)->delete();

        return redirect()->route('course.index');
    } 
}
