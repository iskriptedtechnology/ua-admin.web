<?php

namespace App\Http\Controllers;

use DateTime;
use App\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Repositories\ImageRepositoryInterface;

class AnnouncementController extends Controller
{
    private $_imageRepo;

    /**
     * Constructor
     */
    public function __construct(ImageRepositoryInterface $imageRepo)
    {
        $this->_imageRepo = $imageRepo;
    }

    /**
     * GET
     * Get All Announcements
     * 
     */
    public function index()
    {
        $announcements = Announcement::get();

        return view('announcement.index', compact('announcements'));
    }

    /**
     * GET
     * Get Announcement Details
     * 
     */
    public function view($announcement_id)
    {
        $announcement = Announcement::find($announcement_id);

        return view('announcement.view', compact('announcement'));
    }

    /**
     * GET
     * Show Create Announcement UI
     * 
     */
    public function create()
    {
        return view('announcement.create');
    }

    /**
     * POST
     * Perform Create Announcement
     * 
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required',
            'date'  => 'required|date'
        ]);

        if ($validator->fails()) {

            return redirect()->route('announcement.create')->withErrors($validator)->withInput();
        }

        $create = Announcement::createAnnouncement($request);

        if ($create) {

            if ($request->file('image') != null) {

                $image = $this->_imageRepo->processImage($request);
                $this->_imageRepo->generateImage($image->new_filename, $image->file->getPathName());

                Announcement::updateImage($create->id, $image->new_filename);

            } else {

                Announcement::updateImage($create->id, 'default-50x50.gif');
            }

            return redirect()->route('announcement.index');
        }

        return redirect()->route('announcement.create')->with('status', 'Unable to create announcement. Please try again.')->withInput();
    }

    /**
     * GET
     * Show Edit Announcement UI
     * 
     */

    public function edit($announcement_id)
    {
        $data = Announcement::find($announcement_id);
        $date = new DateTime($data->date);

        $announcement = [
            'id'            => $data->id,
            'name'          => $data->name,
            'description'   => $data->description,
            'date'          => $date->format('m/d/Y'),
            'time'          => $date->format('H:i a'),
            'status'        => (int)$data->status,
            'user'          => $data->user
        ];

        return view('announcement.edit', ['announcement' => $announcement]);
    }

    /**
     * POST
     * Perform Update Announcement
     * 
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required',
            'date'  => 'required|date'
        ]);

        if ($validator->fails()) {

            return redirect()->route('announcement.edit')->withErrors($validator)->withInput();
        }

        $update = Announcement::updateAnnouncement($request);

        if ($update) {

            if ($request->file('image') != null) {

                $image = $this->_imageRepo->processImage($request);
                $this->_imageRepo->generateImage($image->new_filename, $image->file->getPathName());

                Announcement::updateImage($request->id, $image->new_filename);
            }

            return redirect()->route('announcement.index');
        }

        return redirect()->route('announcement.edit')->with('status', 'Unable to update announcement. Please try again.')->withInput();
    }

    /**
     * GET
     * Display Delete Announcement UI
     * params: announcement_id
     */
    public function delete($announcement_id)
    {
        $announcement = Announcement::find($announcement_id);

        return view('announcement.delete', ['announcement' => $announcement]);
    }

    /**
     * DELETE
     * Delete Announcement
     * 
     */
    public function remove($announcement_id)
    {
        $delete = Announcement::where('id', $announcement_id)->delete();

        return redirect()->route('announcement.index');
    }

    /**
     * GET
     * Approve Announcement
     * params: annoucement_id
     */
    public function approve($announcement_id)
    {
        $approve = Announcement::where('id', $announcement_id)->update(['status' => 1]);

        return redirect()->route('announcement.index');
    }

    /**
     * GET
     * Archive Announcement
     * params: annoucement_id
     */
    public function archive($announcement_id)
    {
        $archive = Announcement::where('id', $announcement_id)->update(['status' => 0]);

        return redirect()->route('announcement.index');
    }
}
