<?php

namespace App\Http\Controllers;

use DateTime;
use App\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Repositories\ImageRepositoryInterface;

class EventController extends Controller
{
    private $_imageRepo;

    /**
     * Constructor
     */
    public function __construct(ImageRepositoryInterface $imageRepo)
    {
        $this->_imageRepo = $imageRepo;
    }

    /**
     * GET
     * Get All Events
     * 
     */
    public function index()
    {
        $events = Event::get();

        return view('event.index', compact('events'));
    }

    /**
     * GET
     * Get Event Details
     * 
     */
    public function view($event_id)
    {
        $event = Event::find($event_id);

        return view('event.view', compact('event'));
    }

    /**
     * GET
     * Show Create Event UI
     * 
     */
    public function create()
    {
        return view('event.create');
    }

    /**
     * POST
     * Perform Create Event
     * 
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required',
            'date'  => 'required|date'
        ]);

        if ($validator->fails()) {

            return redirect()->route('event.create')->withErrors($validator)->withInput();
        }

        $create = Event::createEvent($request);

        if ($create) {

            if ($request->file('image') != null) {

                $image = $this->_imageRepo->processImage($request);
                $this->_imageRepo->generateImage($image->new_filename, $image->file->getPathName());

                Event::updateImage($create->id, $image->new_filename);

            } else {

                Event::updateImage($create->id, 'default-50x50.gif');
            }

            return redirect()->route('event.index');
        }

        return redirect()->route('event.create')->with('status', 'Unable to create event. Please try again.')->withInput();
    }

    /**
     * GET
     * Show Edit Event UI
     * 
     */

    public function edit($event_id)
    {
        $data   = Event::find($event_id);
        $date   = new DateTime($data->date);

        $event = [
            'id'            => $data->id,
            'name'          => $data->name,
            'description'   => $data->description,
            'date'          => $date->format('m/d/Y'),
            'time'          => $date->format('H:i a'),
            'status'        => (int)$data->status,
            'user'          => $data->user
        ];

        return view('event.edit', ['event' => $event]);
    }

    /**
     * POST
     * Perform Update Event
     * 
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required',
            'date'  => 'required|date'
        ]);

        if ($validator->fails()) {

            return redirect()->route('event.edit')->withErrors($validator)->withInput();
        }

        $update = Event::updateEvent($request);

        if ($update) {

            if ($request->file('image') != null) {

                $image = $this->_imageRepo->processImage($request);
                $this->_imageRepo->generateImage($image->new_filename, $image->file->getPathName());

                Event::updateImage($request->id, $image->new_filename);
            }

            return redirect()->route('event.index');
        }

        return redirect()->route('event.edit')->with('status', 'Unable to update event. Please try again.')->withInput();
    }

    /**
     * GET
     * Display Delete Event UI
     * params: event_id
     */
    public function delete($event_id)
    {
        $event = Event::find($event_id);

        return view('event.delete', ['event' => $event]);
    }

    /**
     * DELETE
     * Delete Event
     * 
     */
    public function remove($event_id)
    {
        $delete = Event::where('id', $event_id)->delete();

        return redirect()->route('event.index');
    }

    /**
     * GET
     * Approve Event
     * params: event_id
     */
    public function approve($event_id)
    {
        $approve = Event::where('id', $event_id)->update(['status' => 1]);

        return redirect()->route('event.index');
    }

    /**
     * GET
     * Archive Event
     * params: event_id
     */
    public function archive($event_id)
    {
        $archive = Event::where('id', $event_id)->update(['status' => 0]);

        return redirect()->route('event.index');
    }
}
