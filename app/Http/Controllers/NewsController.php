<?php

namespace App\Http\Controllers;

use DateTime;
use App\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Repositories\ImageRepositoryInterface;

class NewsController extends Controller
{
    private $_imageRepo;

    /**
     * Constructor
     */
    public function __construct(ImageRepositoryInterface $imageRepo)
    {
        $this->_imageRepo = $imageRepo;
    }

    /**
     * GET
     * Get All News
     * 
     */
    public function index()
    {
        $newss = News::get();

        return view('news.index', compact('newss'));
    }

    /**
     * GET
     * Get News Details
     * 
     */
    public function view($news_id)
    {
        $news = News::find($news_id);

        return view('news.view', compact('news'));
    }

    /**
     * GET
     * Show Create News UI
     * 
     */
    public function create()
    {
        return view('news.create');
    }

    /**
     * POST
     * Perform Create News
     * 
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'duration_until'    => 'required|date'
        ]);

        if ($validator->fails()) {

            return redirect()->route('news.create')->withErrors($validator)->withInput();
        }

        $create = News::createNews($request);

        if ($create) {

            if ($request->file('image') != null) {

                $image = $this->_imageRepo->processImage($request);
                $this->_imageRepo->generateImage($image->new_filename, $image->file->getPathName());

                News::updateImage($create->id, $image->new_filename);

            } else {

                News::updateImage($create->id, 'default-50x50.gif');
            }

            return redirect()->route('news.index');
        }

        return redirect()->route('news.create')->with('status', 'Unable to create news. Please try again.')->withInput();
    }

    /**
     * GET
     * Show Edit News UI
     * 
     */

    public function edit($news_id)
    {
        $data           = News::find($news_id);
        $duration_until = new DateTime($data->duration_until);

        $news = [
            'id'                => $data->id,
            'name'              => $data->name,
            'description'       => $data->description,
            'duration_until'    => $duration_until->format('m/d/Y'),
            'time'              => $duration_until->format('H:i a'),
            'status'            => (int)$data->status,
            'user'              => $data->user
        ];

        return view('news.edit', ['news' => $news]);
    }

    /**
     * POST
     * Perform Update News
     * 
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'duration_until'    => 'required|date'
        ]);

        if ($validator->fails()) {

            return redirect()->route('news.edit')->withErrors($validator)->withInput();
        }

        $update = News::updateNews($request);

        if ($update) {

            if ($request->file('image') != null) {

                $image = $this->_imageRepo->processImage($request);
                $this->_imageRepo->generateImage($image->new_filename, $image->file->getPathName());

                News::updateImage($request->id, $image->new_filename);
            }

            return redirect()->route('news.index');
        }

        return redirect()->route('news.edit')->with('status', 'Unable to update news. Please try again.')->withInput();
    }

    /**
     * GET
     * Display Delete News UI
     * params: news_id
     */
    public function delete($news_id)
    {
        $news = News::find($news_id);

        return view('news.delete', ['news' => $news]);
    }

    /**
     * DELETE
     * Delete News
     * 
     */
    public function remove($news_id)
    {
        $delete = News::where('id', $news_id)->delete();

        return redirect()->route('news.index');
    }

    /**
     * GET
     * Approve News
     * params: news_id
     */
    public function approve($news_id)
    {
        $approve = News::where('id', $news_id)->update(['status' => 1]);

        return redirect()->route('news.index');
    }

    /**
     * GET
     * Archive News
     * params: news_id
     */
    public function archive($news_id)
    {
        $archive = News::where('id', $news_id)->update(['status' => 0]);

        return redirect()->route('news.index');
    }
}
