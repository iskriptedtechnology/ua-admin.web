<?php

namespace App\Http\Controllers\Api;

use DateTime;
use Carbon\Carbon;
use App\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Repositories\ImageRepositoryInterface;

class AnnouncementController extends Controller
{
    private $_imageRepo;

    /**
     * Constructor
     */
    public function __construct(ImageRepositoryInterface $imageRepo)
    {
        $this->_imageRepo = $imageRepo;
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard('api');
    }

    /**
     * GET
     * Get All Announcements
     * 
     */
    public function getAll()
    {
        $announcements = [];

        $lists = Announcement::where('status', 1)
        ->whereDay('date', '<=', date('d'))
        ->orderBy('date', 'desc')
        ->get();

        if ($lists->count() > 0) {

            foreach ($lists as $list) {

                $announcements[] = [
                    'id'                => $list->id,
                    'name'              => $list->name,
                    'description'       => $list->description,
                    'image'             => URL::to('/').'/assets/img/'.$list->image,
                    'date'              => $list->date->diffForHumans(),
                    'raw_date'          => date('Y-m-d', strtotime($list->date)),
                    'raw_time'          => date('H:i', strtotime($list->date)),
                    'user'              => [
                        'id'            => $list->user->id,
                        'first_name'    => ucwords($list->user->detail->first_name),
                        'last_name'     => ucwords($list->user->detail->last_name),
                        'role'          => ucfirst($list->user->role->name),
                        'image'         => URL::to('/').'/assets/img/'.$list->user->detail->image
                    ]
                ];
            }
        }

        return response()->json(compact('announcements'));
    }


    /**
     * GET
     * Get Announcement Details
     */
    public function getDetails($id)
    {
        $announcement = null;

        $data = Announcement::find($id);

        if ($data) {

            $announcement = [
                'id'                => $data->id,
                'name'              => $data->name,
                'description'       => $data->description,
                'image'             => URL::to('/').'/assets/img/'.$data->image,
                'date'              => $data->date->diffForHumans(),
                'raw_date'          => date('Y-m-d', strtotime($data->date)),
                'raw_time'          => date('H:i', strtotime($data->date)),
                'user'              => [
                    'id'            => $data->user->id,
                    'first_name'    => ucwords($data->user->detail->first_name),
                    'last_name'     => ucwords($data->user->detail->last_name),
                    'role'          => ucfirst($data->user->role->name),
                    'image'         => URL::to('/').'/assets/img/'.$data->user->detail->image
                ]
            ];

            return [
                'status'        => true,
                'announcement'  => $announcement
            ];
        }

        return ['status' => false];
    }

    /**
     * POST
     * Create Announcement
     * 
     */
    public function create(Request $request)
    {
        $date   = new DateTime($request->date.' '.$request->time);
        $date   = $date->format('Y-m-d H:i:s');
        $user   = $this->guard()->user();
        $status = $user->role->id === 3 ? 2 : 1;

        $create = Announcement::create([
            'name'          => $request->name,
            'description'   => $request->description,
            'date'          => $date,
            'status'        => $status,
            'user_id'       => $user->id
        ]);

        if ($create) {

            if ($request->file('image') != null) {

                $image = $this->_imageRepo->processImage($request);
                $this->_imageRepo->generateImage($image->new_filename, $image->file->getPathName());

                Announcement::updateImage($create->id, $image->new_filename);

            } else {

                Announcement::updateImage($create->id, 'default-50x50.gif');
            }

            $announcement = Announcement::find($create->id);

            return response()->json([
                'status'        => true,
                'announcement'  => [
                    'id'                => $announcement->id,
                    'name'              => $announcement->name,
                    'description'       => $announcement->description,
                    'image'             => URL::to('/').'/assets/img/'.$announcement->image,
                    'date'              => $announcement->date->diffForHumans(),
                    'raw_date'          => date('Y-m-d', strtotime($announcement->date)),
                    'raw_time'          => date('H:i', strtotime($announcement->date)),
                    'user'              => [
                        'id'            => $announcement->user->id,
                        'first_name'    => ucwords($announcement->user->detail->first_name),
                        'last_name'     => ucwords($announcement->user->detail->last_name),
                        'role'          => ucfirst($announcement->user->role->name),
                        'image'         => URL::to('/').'/assets/img/'.$announcement->user->detail->image
                    ],
                    'posted'            => Carbon::now() >= $announcement->date
                ]
            ]);
        }

        return response()->json(['status' => false]);
    }

    /**
     * POST
     * Update Announcement
     * 
     */
    public function update(Request $request)
    {
        $update = Announcement::updateAnnouncement($request);

        if ($update) {

            if ($request->file('image') != null) {

                $image = $this->_imageRepo->processImage($request);
                $this->_imageRepo->generateImage($image->new_filename, $image->file->getPathName());

                Announcement::updateImage($request->id, $image->new_filename);
            }

            $announcement = Announcement::find($request->id);

            return response()->json([
                'status' => true,
                'announcement'  => [
                    'id'                => $announcement->id,
                    'name'              => $announcement->name,
                    'description'       => $announcement->description,
                    'image'             => URL::to('/').'/assets/img/'.$announcement->image,
                    'date'              => $announcement->date->diffForHumans(),
                    'raw_date'          => date('Y-m-d', strtotime($announcement->date)),
                    'raw_time'          => date('H:i', strtotime($announcement->date)),
                    'user'              => [
                        'id'            => $announcement->user->id,
                        'first_name'    => ucwords($announcement->user->detail->first_name),
                        'last_name'     => ucwords($announcement->user->detail->last_name),
                        'role'          => ucfirst($announcement->user->role->name),
                        'image'         => URL::to('/').'/assets/img/'.$announcement->user->detail->image
                    ],
                    'posted'            => Carbon::now() >= $announcement->date
                ]
            ]);
        }

        return response()->json([
            'status' => false
        ]);
    }

    /**
     * DELETE
     * Delete Announcement
     * 
     */
    public function delete(Request $request, $id)
    {
        $delete = Announcement::where('id', $id)->delete();

        return response()->json(['status' => true]);
    }

    /**
     * GET
     * Check Announcement if exists
     * 
     */
    public function check(Request $request, $id)
    {
        $announcement = Announcement::find($id);

        return ['status' => (boolean)$announcement];
    }
}
