<?php

namespace App\Http\Controllers\Api;

use App\News;
use DateTime;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Repositories\ImageRepositoryInterface;

class NewsController extends Controller
{
    private $_imageRepo;

    /**
     * Constructor
     */
    public function __construct(ImageRepositoryInterface $imageRepo)
    {
        $this->_imageRepo = $imageRepo;
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard('api');
    }

    /**
     * GET
     * Get All News
     * 
     */
    public function getAll()
    {
        $news = [];

        $lists = News::where([
            'status' => 1,
            ['duration_until', '>=', Carbon::now()]
        ])
        ->orderBy('created_at', 'desc')
        ->get();

        if ($lists->count() > 0) {

            foreach ($lists as $list) {

                $news[] = [
                    'id'                => $list->id,
                    'name'              => $list->name,
                    'description'       => $list->description,
                    'image'             => URL::to('/').'/assets/img/'.$list->image,
                    'date'              => $list->duration_until->diffForHumans(),
                    'raw_date'          => date('Y-m-d', strtotime($list->duration_until)),
                    'raw_time'          => date('H:i', strtotime($list->duration_until)),
                    'user'              => [
                        'id'            => $list->user->id,
                        'first_name'    => ucwords($list->user->detail->first_name),
                        'last_name'     => ucwords($list->user->detail->last_name),
                        'role'          => ucfirst($list->user->role->name),
                        'image'         => URL::to('/').'/assets/img/'.$list->user->detail->image
                    ]
                ];
            }
        }

        return response()->json(compact('news'));
    }

    /**
     * GET
     * Get News Details
     */
    public function getDetails($id)
    {
        $news = null;

        $data = News::find($id);

        if ($data) {

            $news = [
                'id'                => $data->id,
                'name'              => $data->name,
                'description'       => $data->description,
                'image'             => URL::to('/').'/assets/img/'.$data->image,
                'date'              => $data->duration_until->diffForHumans(),
                'raw_date'          => date('Y-m-d', strtotime($data->duration_until)),
                'raw_time'          => date('H:i', strtotime($data->duration_until)),
                'user'              => [
                    'id'            => $data->user->id,
                    'first_name'    => ucwords($data->user->detail->first_name),
                    'last_name'     => ucwords($data->user->detail->last_name),
                    'role'          => ucfirst($data->user->role->name),
                    'image'         => URL::to('/').'/assets/img/'.$data->user->detail->image
                ]
            ];

            return [
                'status'    => true,
                'news'      => $news
            ];
        }

        return ['status' => false];
    }

    /**
     * POST
     * Create News
     * 
     */
    public function create(Request $request)
    {
        $date   = new DateTime($request->duration_until.' '.$request->time);
        $date   = $date->format('Y-m-d H:i:s');
        $user   = $this->guard()->user();
        $status = $user->role->id === 3 ? 2 : 1;

        $create = News::create([
            'name'              => $request->name,
            'description'       => $request->description,
            'duration_until'    => $date,
            'status'            => $status,
            'user_id'           => $user->id
        ]);

        if ($create) {

            if ($request->file('image') != null) {

                $image = $this->_imageRepo->processImage($request);
                $this->_imageRepo->generateImage($image->new_filename, $image->file->getPathName());

                News::updateImage($create->id, $image->new_filename);

            } else {

                News::updateImage($create->id, 'default-50x50.gif');
            }

            $news = News::find($create->id);

            return response()->json([
                'status'        => true,
                'news'  => [
                    'id'                => $news->id,
                    'name'              => $news->name,
                    'description'       => $news->description,
                    'image'             => URL::to('/').'/assets/img/'.$news->image,
                    'date'              => $news->duration_until->diffForHumans(),
                    'raw_date'          => date('Y-m-d', strtotime($news->duration_until)),
                    'raw_time'          => date('H:i', strtotime($news->duration_until)),
                    'user'              => [
                        'id'            => $news->user->id,
                        'first_name'    => ucwords($news->user->detail->first_name),
                        'last_name'     => ucwords($news->user->detail->last_name),
                        'role'          => ucfirst($news->user->role->name),
                        'image'         => URL::to('/').'/assets/img/'.$news->user->detail->image
                    ],
                    'posted'            => Carbon::now() >= $news->date
                ]
            ]);
        }

        return response()->json(['status' => false]);
    }

    /**
     * POST
     * Update News
     * 
     */
    public function update(Request $request)
    {
        $update = News::updateNews($request);

        if ($update) {

            if ($request->file('image') != null) {

                $image = $this->_imageRepo->processImage($request);
                $this->_imageRepo->generateImage($image->new_filename, $image->file->getPathName());

                News::updateImage($request->id, $image->new_filename);
            }

            $news = News::find($request->id);

            return response()->json([
                'status' => true,
                'news'  => [
                    'id'                => $news->id,
                    'name'              => $news->name,
                    'description'       => $news->description,
                    'image'             => URL::to('/').'/assets/img/'.$news->image,
                    'date'              => $news->duration_until->diffForHumans(),
                    'raw_date'          => date('Y-m-d', strtotime($news->duration_until)),
                    'raw_time'          => date('H:i', strtotime($news->duration_until)),
                    'user'              => [
                        'id'            => $news->user->id,
                        'first_name'    => ucwords($news->user->detail->first_name),
                        'last_name'     => ucwords($news->user->detail->last_name),
                        'role'          => ucfirst($news->user->role->name),
                        'image'         => URL::to('/').'/assets/img/'.$news->user->detail->image
                    ],
                    'posted'            => Carbon::now() >= $news->date
                ]
            ]);
        }

        return response()->json([
            'status' => false
        ]);
    }

    /**
     * DELETE
     * Delete News
     * 
     */
    public function delete(Request $request, $id)
    {
        $delete = News::where('id', $id)->delete();

        return response()->json(['status' => true]);
    }

    /**
     * GET
     * Check News if exists
     * 
     */
    public function check(Request $request, $id)
    {
        $news = News::find($id);

        return ['status' => (boolean)$news];
    }
}
