<?php

namespace App\Http\Controllers\Api;

use DateTime;
use App\Event;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Repositories\ImageRepositoryInterface;

class EventController extends Controller
{
    private $_imageRepo;

    /**
     * Constructor
     */
    public function __construct(ImageRepositoryInterface $imageRepo)
    {
        $this->_imageRepo = $imageRepo;
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard('api');
    }

    /**
     * GET
     * Get All Announcements
     * 
     */
    public function getAll()
    {
        $events = [];

        $lists = Event::where('status', 1)
        ->whereDay('date', '<=', date('d'))
        ->orderBy('date', 'desc')
        ->get();

        if ($lists->count() > 0) {

            foreach ($lists as $list) {

                $events[] = [
                    'id'                => $list->id,
                    'name'              => $list->name,
                    'description'       => $list->description,
                    'image'             => URL::to('/').'/assets/img/'.$list->image,
                    'date'              => $list->date->diffForHumans(),
                    'raw_date'          => date('Y-m-d', strtotime($list->date)),
                    'raw_time'          => date('H:i', strtotime($list->date)),
                    'user'              => [
                        'id'            => $list->user->id,
                        'first_name'    => ucwords($list->user->detail->first_name),
                        'last_name'     => ucwords($list->user->detail->last_name),
                        'role'          => ucfirst($list->user->role->name),
                        'image'         => URL::to('/').'/assets/img/'.$list->user->detail->image
                    ]
                ];
            }
        }

        return response()->json(compact('events'));
    }

    /**
     * GET
     * Get Announcement Details
     */
    public function getDetails($id)
    {
        $event = null;

        $data = Event::find($id);

        if ($data) {

            $event = [
                'id'                => $data->id,
                'name'              => $data->name,
                'description'       => $data->description,
                'image'             => URL::to('/').'/assets/img/'.$data->image,
                'date'              => $data->date->diffForHumans(),
                'raw_date'          => date('Y-m-d', strtotime($data->date)),
                'raw_time'          => date('H:i', strtotime($data->date)),
                'user'              => [
                    'id'            => $data->user->id,
                    'first_name'    => ucwords($data->user->detail->first_name),
                    'last_name'     => ucwords($data->user->detail->last_name),
                    'role'          => ucfirst($data->user->role->name),
                    'image'         => URL::to('/').'/assets/img/'.$data->user->detail->image
                ]
            ];

            return [
                'status'    => true,
                'event'     => $event
            ];
        }

        return ['status' => false];
    }

    /**
     * POST
     * Create Event
     * 
     */
    public function create(Request $request)
    {
        $date   = new DateTime($request->date.' '.$request->time);
        $date   = $date->format('Y-m-d H:i:s');
        $user   = $this->guard()->user();
        $status = $user->role->id === 3 ? 2 : 1;

        $create = Event::create([
            'name'          => $request->name,
            'description'   => $request->description,
            'date'          => $date,
            'status'        => $status,
            'user_id'       => $user->id
        ]);

        if ($create) {

            if ($request->file('image') != null) {

                $image = $this->_imageRepo->processImage($request);
                $this->_imageRepo->generateImage($image->new_filename, $image->file->getPathName());

                Event::updateImage($create->id, $image->new_filename);

            } else {

                Event::updateImage($create->id, 'default-50x50.gif');
            }

            $event = Event::find($create->id);

            return response()->json([
                'status'        => true,
                'event'  => [
                    'id'                => $event->id,
                    'name'              => $event->name,
                    'description'       => $event->description,
                    'image'             => URL::to('/').'/assets/img/'.$event->image,
                    'date'              => $event->date->diffForHumans(),
                    'raw_date'          => date('Y-m-d', strtotime($event->date)),
                    'raw_time'          => date('H:i', strtotime($event->date)),
                    'user'              => [
                        'id'            => $event->user->id,
                        'first_name'    => ucwords($event->user->detail->first_name),
                        'last_name'     => ucwords($event->user->detail->last_name),
                        'role'          => ucfirst($event->user->role->name),
                        'image'         => URL::to('/').'/assets/img/'.$event->user->detail->image
                    ],
                    'posted'            => Carbon::now() >= $event->date
                ]
            ]);
        }

        return response()->json(['status' => false]);
    }

    /**
     * POST
     * Update Event
     * 
     */
    public function update(Request $request)
    {
        $update = Event::updateEvent($request);

        if ($update) {

            if ($request->file('image') != null) {

                $image = $this->_imageRepo->processImage($request);
                $this->_imageRepo->generateImage($image->new_filename, $image->file->getPathName());

                Event::updateImage($request->id, $image->new_filename);
            }

            $event = Event::find($request->id);

            return response()->json([
                'status' => true,
                'event'  => [
                    'id'                => $event->id,
                    'name'              => $event->name,
                    'description'       => $event->description,
                    'image'             => URL::to('/').'/assets/img/'.$event->image,
                    'date'              => $event->date->diffForHumans(),
                    'raw_date'          => date('Y-m-d', strtotime($event->date)),
                    'raw_time'          => date('H:i', strtotime($event->date)),
                    'user'              => [
                        'id'            => $event->user->id,
                        'first_name'    => ucwords($event->user->detail->first_name),
                        'last_name'     => ucwords($event->user->detail->last_name),
                        'role'          => ucfirst($event->user->role->name),
                        'image'         => URL::to('/').'/assets/img/'.$event->user->detail->image
                    ],
                    'posted'            => Carbon::now() >= $event->date
                ]
            ]);
        }

        return response()->json([
            'status' => false
        ]);
    }

    /**
     * DELETE
     * Delete Event
     * 
     */
    public function delete(Request $request, $id)
    {
        $delete = Event::where('id', $id)->delete();

        return response()->json(['status' => true]);
    }

    /**
     * GET
     * Check Event if exists
     * 
     */
    public function check(Request $request, $id)
    {
        $event = Event::find($id);

        return ['status' => (boolean)$event];
    }
}
