<?php

namespace App\Http\Controllers\Api;

use App\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['authenticate']]);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard('api');
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticate()
    {
        $credentials = request(['username', 'password']);

        if (! $token = $this->guard()->attempt($credentials)) {

            return response()->json(['error' => 'unauthorized']);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json($this->guard()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $this->guard()->logout();

        return response()->json(['status' => true]);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken($this->guard()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        $user = $this->guard()->user();

        return response()->json([
            'access_token'  => $token,
            'token_type'    => 'bearer',
            'expires_in'    => auth('api')->factory()->getTTL() * 60,
            'user'          => [
                'id'        => $user->id,
                'username'  => $user->username,
                'role'      => $user->role,
                'details'   => $this->_getUserDetails($user)
            ]
        ]);
    }

    /**
     * Get User Details
     * 
     */
    private function _getUserDetails($user)
    {
        if ($user->role->id != 4) {

            return [
                'first_name'        => $user->detail->first_name,
                'middle_name'       => $user->detail->middle_name,
                'last_name'         => $user->detail->last_name,
                'home_address'      => $user->detail->home_address,
                'email'             => $user->detail->email,
                'contact_number'    => $user->detail->contact_number,
                'image'             => url('/assets/img/'.$user->detail->image)
            ];
        }

        $student = Student::where('user_id', $user->id)->first();

        return [
            'first_name'        => $student->first_name,
            'middle_name'       => $student->middle_name,
            'last_name'         => $student->last_name,
            'home_address'      => $student->home_address,
            'email'             => $student->email,
            'contact_number'    => $student->contact_number,
            'gender'            => $student->gender,
            'course'            => $student->course,
            'year_level'        => $student->year_level,
            'image'             => url('/assets/img/'.$student->image)
        ];
    }
}
