<?php

namespace App\Http\Controllers\Api;

use App\News;
use App\Event;
use App\Announcement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GeneralController extends Controller
{
    public function getPendingPostCounts()
    {
        $announcements  = Announcement::where('status', 2)->get();
        $news           = News::where('status', 2)->get();
        $events         = Event::where('status', 2)->get();

        return response()->json([
            'announcements' => (int)$announcements->count(),
            'news'          => (int)$news->count(),
            'events'        => (int)$events->count(),
        ]);
    }
}
