<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * GET
     * Show Login UI
     * 
     */
    public function login()
    {
        if (Auth::check()) {

            return redirect()->intended('/');
        }

        return view('auth.login');
    }

    /**
     * POST
     * Perform Login Action
     * Params: form
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('username', 'password');

        if (Auth::attempt($credentials)) {
            
            return redirect()->intended('/');
        }

        return redirect()->route('login')->withInput()->with('status', 'Invalid Credentials');
    }

    /**
     * POST
     * Perform Logout Action
     * Params: form
     */
    public function logout(Request $request)
    {
        Auth::logout();

        return redirect()->route('login');
    }
}
