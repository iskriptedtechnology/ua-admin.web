<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'home_address',
        'contact_number',
        'email',
        'gender',
        'course_id',
        'year_level',
        'image',
        'user_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * Relationships
     */
    public function course()
    {
        return $this->belongsTo('App\Course');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Custom Methods
     */

    public static function createStudent($request)
    {
        $password = self::_randomString(15);

        $create = $user = User::create([
            'username'      => $request->user_id,
            'password'      => bcrypt($password),
            'user_role_id'  => 4
        ]);

        if ($create) {

            $student = self::create([
                'first_name'        => $request->first_name,
                'middle_name'       => $request->middle_name,
                'last_name'         => $request->last_name,
                'home_address'      => $request->home_address,
                'contact_number'    => $request->contact_number,
                'email'             => $request->email,
                'gender'            => $request->gender,
                'course_id'         => $request->course_id,
                'year_level'        => $request->year_level,
                'user_id'           => $create->id
            ]); 

            $student->username      = $request->user_id;
            $student->password_raw  = $password;

            return $student;
        }
    }

    public static function updateImage($student_id, $new_filename)
    {
        return self::where('id', $student_id)
            ->update([
                'image' => $new_filename
            ]);
    }

    public static function updateStudent($request)
    {
        return self::where('id', $request->id)->update([
            'first_name'        => $request->first_name,
            'middle_name'       => $request->middle_name,
            'last_name'         => $request->last_name,
            'home_address'      => $request->home_address,
            'contact_number'    => $request->contact_number,
            'email'             => $request->email,
            'gender'            => $request->gender,
            'course_id'         => $request->course_id,
            'year_level'        => $request->year_level
        ]);
    }

    private static function _randomString($len)
    {
        $string = "";
        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for ($i=0; $i<$len; $i++) {
            $string.=substr($chars,rand(0,strlen($chars)),1);
        }
            
        return $string;
    }
}
