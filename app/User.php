<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'password',
        'user_role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'created_at',
        'updated_at'
    ];

    /**
     * Relationships
     */
    public function role()
    {
        return $this->belongsTo('App\UserRole', 'user_role_id', 'id');
    }

    public function detail()
    {
        return $this->hasOne('App\UserDetail');
    }

    /**
     * JWT
     */
    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Custom Methods
     */

    /**
     * Create User with Details
     */
    public static function createUser($request)
    {
        $password = self::_randomString(15);

        $create = self::create([
            'username'  => strtolower($request->username),
            'password'  => bcrypt($password),
            'user_role_id'  => $request->user_role_id,
        ]);

        if ($create) {

            UserDetail::create([
                'first_name'        => strtolower($request->first_name),
                'middle_name'       => strtolower($request->middle_name),
                'last_name'         => strtolower($request->last_name),
                'home_address'      => $request->home_address,
                'email'             => strtolower($request->email),
                'contact_number'    => $request->contact_number,
                'user_id'           => $create->id
            ]);
        }

        $user = User::find($create->id);
        $user->password_raw = $password;

        return $user;
    }

    /**
     * Update User with Details
     */
    public static function updateUser($request)
    {
        $update = self::where('id', $request->id)->update([
            'user_role_id'  => $request->user_role_id
        ]);

        if ($update) {

            UserDetail::where('user_id', $request->id)->update([
                'first_name'        => strtolower($request->first_name),
                'middle_name'       => strtolower($request->middle_name),
                'last_name'         => strtolower($request->last_name),
                'home_address'      => $request->home_address,
                'email'             => strtolower($request->email),
                'contact_number'    => $request->contact_number,
            ]);
        }

        return User::find($request->id);
    }

    private static function _randomString($len)
    {
        $string = "";
        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for ($i=0; $i<$len; $i++) {
            $string.=substr($chars,rand(0,strlen($chars)),1);
        }
            
        return $string;
    }
}
