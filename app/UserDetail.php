<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'home_address',
        'email',
        'contact_number',
        'image',
        'user_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * Relationships
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Custom Methods
     */
    
    public static function updateImage($user_id, $new_filename)
    {
        return self::where('user_id', $user_id)
            ->update([
                'image' => $new_filename
            ]);
    }
}
