<?php

namespace App;

use DateTime;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    protected $fillable = [
        'name',
        'description',
        'image',
        'date',
        'status',
        'user_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * Model Relationship
     * 
     */
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    /**
     * Custom Methods
     * 
     */

    public function getDates()
	{
		return ['created_at', 'updated_at', 'date'];
	}
    
    public static function createAnnouncement($request)
    {
        $date   = new DateTime($request->date.' '.$request->time);
        $date   = $date->format('Y-m-d H:i:s');
        $user   = Auth::user();
        $status = $user->role->id === 3 ? 2 : 1;

        return self::create([
            'name'          => $request->name,
            'description'   => $request->description,
            'date'          => $date,
            'status'        => $status,
            'user_id'       => $user->id
        ]);
    }

    public static function updateAnnouncement($request)
    {
        $date   = new DateTime($request->date.' '.$request->time);
        $date   = $date->format('Y-m-d H:i:s');

        return self::where('id', $request->id)->update([
            'name'          => $request->name,
            'description'   => $request->description,
            'date'          => $date
        ]);
    }

    public static function updateImage($announcement_id, $new_filename)
    {
        return self::where('id', $announcement_id)
            ->update([
                'image' => $new_filename
            ]);
    }
}
