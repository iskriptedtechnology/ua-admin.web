<?php

namespace App;

use DateTime;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';

    protected $fillable = [
        'name',
        'description',
        'image',
        'duration_until',
        'status',
        'user_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * Model Relationship
     * 
     */
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    /**
     * Custom Methods
     * 
     */

    public function getDates()
	{
		return ['created_at', 'updated_at', 'duration_until'];
	}
    
    public static function createNews($request)
    {
        $duration_until     = new DateTime($request->duration_until.' '.$request->time);
        $duration_until     = $duration_until->format('Y-m-d H:i:s');
        $user               = Auth::user();
        $status             = $user->role->id === 3 ? 2 : 1;

        return self::create([
            'name'              => $request->name,
            'description'       => $request->description,
            'duration_until'    => $duration_until,
            'status'            => $status,
            'user_id'           => $user->id
        ]);
    }

    public static function updateNews($request)
    {
        $duration_until   = new DateTime($request->duration_until.' '.$request->time);
        $duration_until   = $duration_until->format('Y-m-d H:i:s');

        return self::where('id', $request->id)->update([
            'name'              => $request->name,
            'description'       => $request->description,
            'duration_until'    => $duration_until
        ]);
    }

    public static function updateImage($news_id, $new_filename)
    {
        return self::where('id', $news_id)
            ->update([
                'image' => $new_filename
            ]);
    }
}
