<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
        'name',
        'description',
        'years'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * Relationships
     */
    public function student()
    {
        return $this->belongsTo('App\Student');
    }

    /**
     * Custom Methods
     */

    public static function createCourse($request)
    {
        return self::create([
            'name'          => $request->name,
            'description'   => $request->description,
            'years'         => $request->years
        ]);
    }

    public static function updateCourse($request)
    {
        return self::where('id', $request->id)->update([
            'name'          => $request->name,
            'description'   => $request->description,
            'years'         => $request->years
        ]);
    }
}
